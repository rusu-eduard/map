package socialnetwork.service;

import socialnetwork.domain.*;
import socialnetwork.utils.events.ChangeEventType;
import socialnetwork.utils.events.PrietenieChangeEvent;
import socialnetwork.utils.observer.Observer;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

public class ServiceWithUser {
    private Utilizator user;
    private UtilizatorService service;
    private int notifications = 0;

    public ServiceWithUser(Utilizator user, UtilizatorService service) {
        this.user = user;
        this.service = service;
    }

    public int getNotifications(){
        return notifications;
    }

    public void incNotification(){
        notifications++;
        service.notifyObservers(new PrietenieChangeEvent(ChangeEventType.NOTIFICATION));
    }

    public Utilizator getUser() {
        return user;
    }

    public UtilizatorService getService() {
        return service;
    }

    public void addFriend(Utilizator other){
        service.sendRequest(user.getId(), other.getId());
    }

    public void removeFriend(DTOUtilizator other){
        service.removeFriendship(user.getId(), other.getId());
    }

    public Iterable<FriendRequest> getFriendRequests(){
        return service.getAllFromToUser(user.getId());
    }

    public Iterable<FriendRequest> getFriendRequests(int pageNumber, int pageSize){
        return service.getAllFromToUser(user.getId(), pageNumber, pageSize);
    }

    public void acceptRequest(Utilizator other){
        service.acceptRequest(other.getId(), this.user.getId());
    }

    public void cancelRequest(Utilizator other){
        service.deleteRequest(this.user.getId(), other.getId());
    }

    public void declineRequest(Utilizator other){
        service.deleteRequest(other.getId(), this.user.getId());
    }

    public Iterable<Utilizator> getPendingRequests(){
        return StreamSupport.stream(service.getRequestsFor(user.getId()).spliterator(),false)
                .filter(x->x.getStatus().equals(FriendRequestStatus.PENDING))
                .map(x->service.findOne(x.getFrom()))
                .collect(Collectors.toList());
    }

    public Iterable<DTOUtilizator> getFriends(){
        return service.getDTOFriends(user.getId(), 0,false);
    }

    public Iterable<DTOUtilizator> getFriends(int pageNumber, int pageSize, String nume){
        return service.getDTOFriends(user.getId(), pageNumber, pageSize, nume);
    }

    public Iterable<PendingRequestUser> getPossibleFriends(){
        Predicate<Utilizator> notThis = x -> !x.getId().equals(this.user.getId());
        Predicate<Utilizator> notFriends = x -> service.findOneFriendship(this.user.getId(), x.getId()) == null;
        Predicate<Utilizator> mainPredicate = notThis.and(notFriends);
        return StreamSupport.stream(service.getAll().spliterator(), false)
                .filter(mainPredicate)
                .map(x ->{
                    if(service.FindRequestBetween(x.getId(),this.user.getId()))
                        return new PendingRequestUser(x, true);
                    return new PendingRequestUser(x, false);
                })
                .collect(Collectors.toList());
    }

    public Iterable<PendingRequestUser> getPossibleFriends(int pageNumber, int pageSize, String name){
        return StreamSupport.stream(service.getPossibleFriends(user.getId(), pageNumber, pageSize, name).spliterator(), false)
                .map(x ->{
                    if(service.FindRequestBetween(x.getId(), this.user.getId()))
                        return new PendingRequestUser(x, true);
                    return new PendingRequestUser(x, false);
                })
                .collect(Collectors.toList());
    }

    public void addObserver(Observer<PrietenieChangeEvent> e){
        service.addObserver(e);
    }

    public Utilizator findOne(Long id){
        return service.findOne(id);
    }

    public Long getNrOfFriends(){
        long count = 0;
        for(DTOUtilizator f : getFriends()){
            count++;
        }
        return count;
    }

    public Iterable<MessageDTO> getMessagesWithUser(Long uId){
        return service.getMsgBetweenUsers(uId, user.getId());
    }

    public Iterable<MessageDTO> getMessagesWithUser(Long uId, int pageNumber, int pageSize){
        return service.getMsgBetweenUsers(uId, user.getId(), pageNumber, pageSize);
    }

    public void sendMessageToUser(String message, Long to){
        service.sendMessage(user.getId(), message, to, null);
    }

    public void replyMessageFromUser(String message, Long mId){
        service.replyMessageFromUser(mId, message, user.getId());
    }

    public Iterable<Utilizator> getUsersWithConv(){
        return service.getUsersWithConv(user.getId());
    }

    public Iterable<Utilizator> getAllExceptMe(){
        return service.getAllExceptOne(user.getId());
    }

    public void createGroup(String groupName, List<Long> members) {
        members.add(this.getUser().getId());
        service.createGroup(groupName, members);
    }

    public Iterable<Group> getMyGroups(){
        return service.getGroupsWithUser(this.user.getId());
    }

    public Iterable<MessageDTO> getMessagesWithGroup(Long id) {
        return service.getMessagesWithGroup(id);
    }

    public Iterable<MessageDTO> getMessagesWithGroup(Long id, int pageNumber, int pageSize) {
        return service.getMessagesWithGroup(id, pageNumber, pageSize);
    }

    public void sendMessageToGroup(String message, Long id) {
        service.sendMessage(this.user.getId(), message, null, id);
    }


    public void replyMessageFromGroup(String message, Long id) {
        service.replyMessageFromGroup(id, message, user.getId());
    }

    public Iterable<Message> getMessagesToMe(){
        return service.getMessagesTo(user.getId());
    }

    public UserPage getUserPage(){
        long uId = this.user.getId();
        String lastName = this.user.getLastName();
        String firstName = this.user.getFirstName();
        Long nrOfFriends = this.getNrOfFriends();
        List<DTOUtilizator> friends = StreamSupport.stream(this.getFriends(1,2,"").spliterator(), false)
                .collect(Collectors.toList());

        List<Utilizator> usersWithConv = StreamSupport.stream(this.getUsersWithConv().spliterator(), false)
                .collect(Collectors.toList());

        Map<Utilizator, List<MessageDTO>> conversations = new HashMap<>();

        for (Utilizator user : usersWithConv){
            List<MessageDTO> conv = StreamSupport.stream(this.getMessagesWithUser(user.getId()).spliterator(), false)
                            .collect(Collectors.toList());
            conversations.put(user, conv);
        }

        List<Group> groups = StreamSupport.stream(this.getMyGroups().spliterator(), false)
                .collect(Collectors.toList());

        Map<Group, List<MessageDTO>> groupConv = new HashMap<>();

        for(Group g : groups){
            List<MessageDTO> conv = StreamSupport.stream(this.getMessagesWithGroup(g.getId()).spliterator(), false)
                    .collect(Collectors.toList());
            groupConv.put(g, conv);
        }

        List<Utilizator> pendingRequests = StreamSupport.stream(this.getPendingRequests().spliterator(), false)
                .collect(Collectors.toList());

        List<FriendRequest> requestHistory = StreamSupport.stream(this.getFriendRequests(1, 2).spliterator(), false)
                .collect(Collectors.toList());

        return new UserPage(uId, lastName, firstName, nrOfFriends, friends, conversations, groupConv, pendingRequests, requestHistory);
    }

}
