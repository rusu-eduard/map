package socialnetwork.service;

import socialnetwork.domain.*;
import socialnetwork.exceptions.DBException;
import socialnetwork.exceptions.FriendshipException;
import socialnetwork.exceptions.ServiceException;
import socialnetwork.repository.Repository;
import socialnetwork.repository.database.DBEventsRepo;
import socialnetwork.repository.database.DBMessageRepo;
import socialnetwork.repository.database.DBRequestsRepo;
import socialnetwork.utils.events.ChangeEventType;
import socialnetwork.utils.events.PrietenieChangeEvent;
import socialnetwork.utils.observer.Observable;
import socialnetwork.utils.observer.Observer;

import java.time.LocalDateTime;
import java.util.*;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

public class UtilizatorService implements Observable<PrietenieChangeEvent> {
    private final Repository<Long, Utilizator> repo;
    private final Repository<Tuple<Long,Long>,Prietenie> network;
    private final DBMessageRepo messageDBRepo;
    private final DBRequestsRepo requestsRepo;
    private final DBEventsRepo eventsRepo;
    private final List<Observer<PrietenieChangeEvent>> observers = new ArrayList<>();

    public UtilizatorService(Repository<Long, Utilizator> repo, Repository<Tuple<Long, Long>, Prietenie> network, DBMessageRepo messageDBRepo, DBRequestsRepo requestsRepo, DBEventsRepo eventsRepo) {
        this.repo = repo;
        this.network = network;
        this.messageDBRepo = messageDBRepo;
        this.requestsRepo = requestsRepo;
        this.eventsRepo = eventsRepo;
    }

    /**
     * Adds an user to repo
     * @param utilizator user to be added
     *                   must not be null
     * @return null if the user is saved, otherwise returns the entity(Id already exists)
     */
    public Utilizator addUtilizator(Utilizator utilizator) {
        utilizator.setPassword(utilizator.encryptPassword());
        Utilizator user = repo.save(utilizator);
        notifyObservers(new PrietenieChangeEvent(ChangeEventType.ADDUSER));
        return user;
    }

    /**
     *
     * @return All users
     */
    public Iterable<Utilizator> getAll(){
        return repo.findAll();
    }

    public Iterable<Utilizator> getAllExceptOne(Long id) {
        return messageDBRepo.getAllExceptOne(id);
    }

    /**
     * Creates an utilizator and adds it to the repository
     * @param name - name of the user to be added
     * @param surname - surname of the user to be added
     * @return null if the user is added successfully, otherwise returns an User(Id already exists)
     */
    public Utilizator save(String name, String surname, String password) {
        Utilizator user = new Utilizator(name, surname, password);
        return addUtilizator(user);
    }

    /**
     * Removes an user from the repository
     * @param id - id of user to be removed
     * @return - the removed user, or null if there is no user with the given id
     * @throws RuntimeException if it fails to delete all the friendships
     */
    public Utilizator removeUser(Long id) {
        return repo.delete(id);
    }

    /**
     * Adds a friendship relation between two users
     * @param id1 - id of 1st user
     * @param id2 - id of 2nd user
     * @return null if the friendship is saved, Prietenie if a friendship between the two users already exists
     */
    public Prietenie addFriendship(long id1, long id2) {
        if(repo.findOne(id1) == null){
            throw new FriendshipException("The 1st user does not exist!");
        }
        if(repo.findOne(id2) == null){
            throw new FriendshipException("The 2nd user does not exist!");
        }
        if(id1 == id2){
            throw new FriendshipException("The users must be different from one another!");
        }

        Prietenie p;

        if(id1 < id2)
            p = new Prietenie(id1, id2);
        else
            p = new Prietenie(id2, id1);
        if(network.findOne(p.getId()) == null){
            return network.save(p);
        }
        else
            throw new FriendshipException("Friendship already exists");
    }

    public  Prietenie findOneFriendship(Long id1, Long id2){
        Prietenie p;
        if(id1 < id2)
            p = new Prietenie(id1, id2);
        else
            p = new Prietenie(id2, id1);
        return network.findOne(p.getId());
    }

    /**
     *
     * @return Iterable object that contains all the friendship relations
     */
    public Iterable<Prietenie> getAllFriendships() {
        return network.findAll();
    }

    /**
     * Finds an user form repository
     * @param id1 id of searched user
     * @return the user with the given id or null if there is no user with that id
     */
    public Utilizator findOne(Long id1) {
        return repo.findOne(id1);
    }

//    /**
//     * Method that updates all friendship relations from network into the repository
//     */
//    private void loadAllData(){
//        network.findAll().forEach(this::loadFriendship);
//    }
//
//    /**
//     * loads a new friendship into the User`s "Friends" field
//     * @param e - friendship
//     */
//    private void loadFriendship(Prietenie e){
//        Tuple<Long, Long> id = e.getId();
//        repo.findOne(id.getLeft()).addFriend(id.getRight());
//        repo.findOne(id.getRight()).addFriend(id.getLeft());
//    }

    /**
     * removes a friendship relation between 2 users
     * @param id1 id of 1st user
     * @param id2 id of 2nd user
     * @return The friendship that was removed, or null if there was no friendship relation between the 2 users
     */
    public Prietenie removeFriendship(Long id1, Long id2) {
        Tuple<Long, Long> id;
        if(id1 < id2){
            id = new Tuple<>(id1, id2);
        }
        else{
            id = new Tuple<>(id2, id1);
        }
        Prietenie p = network.delete(id);
        if(p!=null){
            notifyObservers(new PrietenieChangeEvent(ChangeEventType.DELETE));
        }
        return p;
    }

    /**
     * Returns a list with all the friends of u
     * @param u - user
     * @return all friends of user u
     */
    private Iterable<Utilizator> getFriends(Utilizator u){
        ArrayList<Utilizator> friends = new ArrayList<>();
        StreamSupport.stream(network.findAll().spliterator(),false)
                .filter(y ->
                    y.getId().getLeft().equals(u.getId()) || y.getId().getRight().equals(u.getId())
                )
                .forEach(y ->{
                    Utilizator fr;
                    if(y.getId().getLeft().equals(u.getId())){
                        fr = repo.findOne(y.getId().getRight());
                    }
                    else
                        fr = repo.findOne(y.getId().getLeft());
                    friends.add(fr);
                });
        return friends;
    }

    /**
     * @param id - Long - id of a user
     * @return Returns the list of friends of the user with id id
     */
    public Iterable<DTOUtilizator> getDTOFriends(Long id, int month, boolean byMonth){
//        Utilizator u = findOne(id);
//        Predicate<Prietenie> byUser = x -> x.getId().getRight().equals(u.getId()) || x.getId().getLeft().equals(u.getId());
//        Predicate<Prietenie> byUserAndMonth = byUser.and(x->x.getDate().getMonthValue() == month);
//        Predicate<Prietenie> mainPredicate;
//        if(!byMonth)
//            mainPredicate = byUser;
//        else
//            mainPredicate = byUserAndMonth;
//        if(byMonth && (month < 0 || month > 12)){
//            throw new ServiceException("Luna este invalida");
//        }
//        return StreamSupport.stream(network.findAll().spliterator(), false)
//                .filter(mainPredicate)
//                .map(x -> {
//                    Utilizator fr;
//                    if(x.getId().getLeft().equals(u.getId())){
//                        fr = repo.findOne(x.getId().getRight());
//                    }
//                    else
//                        fr = repo.findOne(x.getId().getLeft());
//                    return new DTOUtilizator(fr.getId(), fr.getFirstName(), fr.getLastName(), x.getDate());
//                })
//                .collect(Collectors.toSet());
        return messageDBRepo.getFriendsForUser(id);
    }

    public Iterable<DTOUtilizator> getDTOFriends(Long id, int pageNumber, int pageSize, String nume){
//        Utilizator u = findOne(id);
//        Predicate<Prietenie> byUser = x -> x.getId().getRight().equals(u.getId()) || x.getId().getLeft().equals(u.getId());
//        Predicate<Prietenie> byUserAndMonth = byUser.and(x->x.getDate().getMonthValue() == month);
//        Predicate<Prietenie> mainPredicate;
//        if(!byMonth)
//            mainPredicate = byUser;
//        else
//            mainPredicate = byUserAndMonth;
//        if(byMonth && (month < 0 || month > 12)){
//            throw new ServiceException("Luna este invalida");
//        }
//        return StreamSupport.stream(network.findAll().spliterator(), false)
//                .filter(mainPredicate)
//                .map(x -> {
//                    Utilizator fr;
//                    if(x.getId().getLeft().equals(u.getId())){
//                        fr = repo.findOne(x.getId().getRight());
//                    }
//                    else
//                        fr = repo.findOne(x.getId().getLeft());
//                    return new DTOUtilizator(fr.getId(), fr.getFirstName(), fr.getLastName(), x.getDate());
//                })
//                .collect(Collectors.toSet());
        return messageDBRepo.getFriendsForUser(id, pageNumber, pageSize, nume);
    }

    /**
     *
     * @return number of social communities
     */
    public long getCommunities(){
        Map<Utilizator, Long> distances = new HashMap<>();
        long defaultDistance = -999;
        getAll().forEach(e -> distances.putIfAbsent(e, defaultDistance));
        long count = 0;
        Iterable<Utilizator> users = getAll();
        for(Utilizator u : users){
            if(distances.get(u).equals(defaultDistance)){
                count++;
                DFS(distances, u);
            }
        }
        return count;
    }

    /**
     *
     * @return the longest community
     */
    public Iterable<Utilizator> getLongestCommunity(){
        Map<Utilizator, Long> distances = new HashMap<>();
        ArrayList<Utilizator> rez = new ArrayList<>();
        long defaultDistance = -999;
        long maxDistance = -1;
        getAll().forEach(e -> distances.putIfAbsent(e, defaultDistance));
        ArrayList<Utilizator> users = new ArrayList<>(distances.keySet());

        for(Utilizator user : users) {

            DFS(distances, user);
            Set<Map.Entry<Utilizator, Long>> set = distances.entrySet();
            long max = -1;

            for (Map.Entry<Utilizator, Long> entry : set) {
                if (entry.getValue() > max)
                    max = entry.getValue();
            }

            if(max > maxDistance){
                maxDistance = max;
                rez.clear();
                for (Map.Entry<Utilizator, Long> entry2 : set) {
                    if (!entry2.getValue().equals(defaultDistance)) {
                        rez.add(entry2.getKey());
                    }
                }
            }

            for (Map.Entry<Utilizator, Long> entry : set){
                if (!entry.getValue().equals(defaultDistance)) {
                    distances.replace(entry.getKey(), defaultDistance);
                }
            }

        }
        return rez;
    }

    /**
     * Depth first search algorithm
     * @param distances a map containing the distance between the nodes of the graph
     * @param u the node from where the search starts
     */
    private void DFS(Map<Utilizator, Long> distances, Utilizator u) {
        Stack<Utilizator> stack= new Stack<>();
        long distance = 0;
        long defaultDistance = -999;
        stack.push(u);
        while(!stack.empty()){
            Utilizator user = stack.pop();
            if(distances.get(user).equals(defaultDistance)){
                distances.replace(user,distance);
                getFriends(user).forEach(stack::push);
            }
            distance++;
        }
    }

    /**
     * Sends a message from one user to one or many others
     * @param from id of the user who sends the message
     * @param message the message
     * @throws ServiceException if one of the users is not found
     */
    public void sendMessage(Long from, String message, Long toUser, Long toGroup){
        Message m = new Message(from, message, toUser, toGroup);
        messageDBRepo.save(m);
        notifyObservers(new PrietenieChangeEvent(ChangeEventType.SENTMESSAGE));
    }

    public Message getOneMessage(Long mid){
        return messageDBRepo.getMessage(mid);
    }

    public void replyMessageFromUser(Long mId, String message, Long user){
        Message m = messageDBRepo.getMessage(mId);
        if(m == null){
            throw new ServiceException("Message does not exist!");
        }
        else{
            Message reply = new Message(user, message, mId, m.getFrom(), null);
            messageDBRepo.save(reply);
            notifyObservers(new PrietenieChangeEvent(ChangeEventType.SENTMESSAGE));
        }
    }

    /**
     * One user replies to everyone who received the message with id mId
     * @param mId Long -> id of the message
     * @param message String -> reply message
     * @param user Long -> id of user who replies
     * @throws ServiceException if the user does not exist or if he did not receive a message with id mId
     */
    public void replyAllMessage(Long mId, String message, Long user){
        //TODO
    }

    /**
     * One user replies to another user who received the message with id mId
     * @param mId Long -> id of the message
     * @param message String -> reply message
     * @param replyUser Long -> id of user who replies
     * @param replyToUser Long -> id of user who is replied to
     */
    public void replyOneMessage(Long mId, String message, Long replyUser, Long replyToUser){
        //TODO
    }

    public Iterable<Utilizator>getUsersWithConv(Long userId){
        return StreamSupport.stream(messageDBRepo.getUsersWithConv(userId).spliterator(), false)
                .map(this::findOne)
                .collect(Collectors.toList());
    }

    /**
     *
     * @param id1 Long -> id of one user
     * @param id2 Long -> id of another user
     * @return an Iterable object containing all the messages between user with id id1 and user with id id2 sorted by date
     * @throws ServiceException if one of the 2 users does not exist or if they are the same person
     */
    public Iterable<MessageDTO> getMsgBetweenUsers(Long id1, Long id2){
        if(repo.findOne(id1) == null){
            throw new ServiceException("User 1 does not exist");
        }
        if(repo.findOne(id2) == null){
            throw new ServiceException("User 2 does not exist");
        }
        if(id1.equals(id2)){
            throw new ServiceException("Users must be different people");
        }
        return StreamSupport.stream(messageDBRepo.getMsgBetweenUsers(id1, id2).spliterator(), false)
                .map(x->{
                    System.out.println(x.getReply());
                    if(x.getReply() == 0)
                        return new MessageDTO(findOne(x.getFrom()).getLastName(), x.getMessage(), x.getDate(), findOne(x.getToUser()).getLastName(), x.getId());
                    else{
                        String replyToMsg = getOneMessage(x.getReply()).getMessage();
                        return new MessageDTO(findOne(x.getFrom()).getLastName(), x.getMessage(), replyToMsg, x.getDate(), findOne(x.getToUser()).getLastName(), x.getId());
                    }
                })
                .sorted(Comparator.comparing(MessageDTO::getDate))
                .collect(Collectors.toList());
    }

    public Iterable<MessageDTO> getMsgBetweenUsers(Long id1, Long id2, int pageNumber, int pageSize){
        if(repo.findOne(id1) == null){
            throw new ServiceException("User 1 does not exist");
        }
        if(repo.findOne(id2) == null){
            throw new ServiceException("User 2 does not exist");
        }
        if(id1.equals(id2)){
            throw new ServiceException("Users must be different people");
        }
        return StreamSupport.stream(messageDBRepo.getMsgBetweenUsers(id1, id2, pageNumber, pageSize).spliterator(), false)
                .map(x->{
                    System.out.println(x.getReply());
                    if(x.getReply() == 0)
                        return new MessageDTO(findOne(x.getFrom()).getLastName(), x.getMessage(), x.getDate(), findOne(x.getToUser()).getLastName(), x.getId());
                    else{
                        String replyToMsg = getOneMessage(x.getReply()).getMessage();
                        return new MessageDTO(findOne(x.getFrom()).getLastName(), x.getMessage(), replyToMsg, x.getDate(), findOne(x.getToUser()).getLastName(), x.getId());
                    }
                })
                .sorted(Comparator.comparing(MessageDTO::getDate))
                .collect(Collectors.toList());
    }

    /**
     * Sends a friend request from one user to another
     * @param idFrom Long -> id of user who sends the request
     * @param idTo Long -> id of user who receives the request
     * @throws ServiceException if:
     *                  One of the 2 users does not exist
     *                  There is already a pending friend request between them
     *                  They are already friends
     */
    public void sendRequest(long idFrom, long idTo) {
        if(repo.findOne(idFrom)==null){
            throw new ServiceException("Did not foud user with id " + idFrom);
        }
        if(repo.findOne(idTo) == null){
            throw new ServiceException("Did not found user with id " + idTo);
        }
        if(requestsRepo.findOne(idTo,idFrom) != null){
            throw new ServiceException("There already is a pending request between these users");
        }
        if(requestsRepo.findOne(idFrom,idTo) != null){
            throw new ServiceException("There already is a pending request between these users");
        }
        if(network.findOne(new Tuple<>(idFrom, idTo)) != null){
            throw new ServiceException("These users are already friends");
        }
        requestsRepo.save(new FriendRequest(idFrom, idTo));
        notifyObservers(new PrietenieChangeEvent(ChangeEventType.SENDREQUEST));
    }

    /**
     * Adds a friendship relation between user with id=idFrom and user with id=idTo
     * @param idFrom Long -> user who sent the request
     * @param idTo Long -> user who received the request
     * @throws ServiceException if:
     *                  -One of the 2 users does not exist
     *                  -There is no friend request between them
     */
    public void acceptRequest(long idFrom, long idTo){
        if(repo.findOne(idFrom)==null){
            throw new ServiceException("Did not foud user with id " + idFrom);
        }
        if(repo.findOne(idTo) == null){
            throw new ServiceException("Did not found user with id " + idTo);
        }
        FriendRequest request = requestsRepo.findOne(idTo,idFrom);
        if(request == null){
            throw new ServiceException("There is no pending friend request between these users");
        }
        requestsRepo.updateRequest(request,FriendRequestStatus.APPROVED);
        addFriendship(idTo, idFrom);
        notifyObservers(new PrietenieChangeEvent(ChangeEventType.ACCEPTREQUEST));
    }

    /**
     * Removes a friend request between 2 users
     * @param idFrom id of a user
     * @param idTo id of other user
     * @throws ServiceException if:
     *          - Users does not exist
     *          -Friend request does not exist
     */
    public void deleteRequest(long idFrom, long idTo){
        if(repo.findOne(idFrom)==null){
            throw new ServiceException("Did not foud user with id " + idFrom);
        }
        if(repo.findOne(idTo) == null){
            throw new ServiceException("Did not found user with id " + idTo);
        }
        FriendRequest request = requestsRepo.findOne(idTo,idFrom);
        if(request == null){
            throw new ServiceException("There is no friend request between these users");
        }
        requestsRepo.updateRequest(request,FriendRequestStatus.DECLINED);
        notifyObservers(new PrietenieChangeEvent(ChangeEventType.DECLINEREQUEST));
    }

    /**
     *Gets all friend request for a user
     * @param idTo id of a user
     * @return all friend requests of that user
     * @throws ServiceException if the user does not exist
     */
    public Iterable<FriendRequest> getRequestsFor(long idTo){
        if(repo.findOne(idTo) == null){
            throw new ServiceException("Did not found user with id " + idTo);
        }
        return requestsRepo.findAllFor(idTo);
    }

    public Boolean FindRequestBetween(long idTo, long idFrom){
        return requestsRepo.findOne(idTo, idFrom) != null;
    }

    @Override
    public void addObserver(Observer<PrietenieChangeEvent> e) {
        observers.add(e);
    }

    @Override
    public void removeObserver(Observer<PrietenieChangeEvent> e) {
        observers.remove(e);
    }

    @Override
    public void notifyObservers(PrietenieChangeEvent t) {
        observers.forEach(x->x.update(t));
    }

    public Iterable<FriendRequest> getAllFromToUser(long id){
        if(repo.findOne(id) == null){
            throw new ServiceException("Did not found user with id " + id);
        }
        return requestsRepo.findAllFromTo(id);
    }

    public Iterable<FriendRequest> getAllFromToUser(long id, int pageNumber, int pageSize){
        if(repo.findOne(id) == null){
            throw new ServiceException("Did not found user with id " + id);
        }
        return messageDBRepo.getAllFromTo(id, pageNumber, pageSize);
    }

    public void saveEvent(Event ev){
        eventsRepo.save(ev);
        notifyObservers(new PrietenieChangeEvent(ChangeEventType.NEWEVENT));
    }

    public void addParticipantToEv(Event ev, Long id){
        eventsRepo.addParticipantToEv(ev, id);
        notifyObservers(new PrietenieChangeEvent(ChangeEventType.EVENTSUBSCRIPTION));
    }

    public Iterable<Event> getEventsForUser(Long id){
        return eventsRepo.getEventsForUser(id);
    }

    public Iterable<Event> getAllEvents(){
        return eventsRepo.getEvents();
    }

    public void unnotifyUser(Event ev, Long id) {
        eventsRepo.unnotifyUser(ev.getId(), id);
        notifyObservers(new PrietenieChangeEvent(ChangeEventType.EVENTSUBSCRIPTION));
    }

    public void notifyUser(Event ev, Long id) {
        eventsRepo.notifyUser(ev.getId(), id);
        notifyObservers(new PrietenieChangeEvent(ChangeEventType.EVENTSUBSCRIPTION));
    }

    public void removeParticipantFromEv(Event ev, Long id) {
        eventsRepo.removeParticipantFromEv(ev,id);
        notifyObservers(new PrietenieChangeEvent(ChangeEventType.EVENTSUBSCRIPTION));
    }

    public void createGroup(String groupName, List<Long> members) {
        Group gr = new Group(groupName, members);
        messageDBRepo.saveGroup(gr);
        notifyObservers(new PrietenieChangeEvent(ChangeEventType.NEWGROUP));
    }

    public Iterable<Group> getGroupsWithUser(Long id){
        return messageDBRepo.getGroupsWithUser(id);
    }

    public Group getGroup(long id){
        return messageDBRepo.getGroup(id);
    }

    public Iterable<MessageDTO> getMessagesWithGroup(Long id) {
        return StreamSupport.stream(messageDBRepo.getMessagesForGroup(id).spliterator(), false)
                .map(x->{
                    String from = findOne(x.getFrom()).getLastName();
                    String msg = x.getMessage();
                    LocalDateTime time = x.getDate();
                    String to = getGroup(x.getToGroup()).getName();
                    Long mId = x.getId();
                    Long reply = x.getReply();
                    if(reply == 0)
                        return new MessageDTO(from, msg, time, to, mId);
                    else{
                        String replyToMsg = getOneMessage(x.getReply()).getMessage();
                        return new MessageDTO(from, msg, replyToMsg, time, to, mId);
                    }

                })
                .sorted(Comparator.comparing(MessageDTO::getDate))
                .collect(Collectors.toList());
    }

    public Iterable<MessageDTO> getMessagesWithGroup(Long id, int pageNumber, int pageSize) {
        return StreamSupport.stream(messageDBRepo.getMessagesForGroup(id, pageNumber, pageSize).spliterator(), false)
                .map(x->{
                    String from = findOne(x.getFrom()).getLastName();
                    String msg = x.getMessage();
                    LocalDateTime time = x.getDate();
                    String to = getGroup(x.getToGroup()).getName();
                    Long mId = x.getId();
                    Long reply = x.getReply();
                    if(reply == 0)
                        return new MessageDTO(from, msg, time, to, mId);
                    else{
                        String replyToMsg = getOneMessage(x.getReply()).getMessage();
                        return new MessageDTO(from, msg, replyToMsg, time, to, mId);
                    }

                })
                .sorted(Comparator.comparing(MessageDTO::getDate))
                .collect(Collectors.toList());
    }

    public void replyMessageFromGroup(Long id, String message, Long id1) {
        Message m = messageDBRepo.getMessage(id);
        Message reply = new Message(id1, message, id, null, m.getToGroup());
        messageDBRepo.save(reply);
    }

    public Iterable<Message> getMessagesTo(Long id){
        return messageDBRepo.getMessagesTo(id);
    }

    public Iterable<Utilizator> getPossibleFriends(Long id, int pageNumber, int pageSize, String name) {
        return messageDBRepo.getPossibleFriends(id, pageNumber, pageSize, name);
    }

    public void updateEvent(Long evId, EventStatus status){
        eventsRepo.updateEvStatus(evId, status);
        notifyObservers(new PrietenieChangeEvent(ChangeEventType.NEWEVENT));
    }
}
