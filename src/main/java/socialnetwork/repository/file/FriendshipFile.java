package socialnetwork.repository.file;

import socialnetwork.domain.Prietenie;
import socialnetwork.domain.Tuple;

import java.time.LocalDateTime;
import java.util.List;

public class FriendshipFile extends AbstractFileRepository<Tuple<Long,Long>, Prietenie>{
    public FriendshipFile(String fileName1) {
        super(fileName1);
    }

    @Override
    public Prietenie extractEntity(List<String> attr) {
        return new Prietenie(Long.parseLong(attr.get(0)),Long.parseLong(attr.get(1)), LocalDateTime.parse(attr.get(2)));
    }

    @Override
    protected String createEntityAsString(Prietenie entity) {
        return entity.getId()+";"+ entity.getDate();
    }
}
