package socialnetwork.repository.file;

import socialnetwork.domain.Entity;
import socialnetwork.domain.validators.Validator;
import socialnetwork.exceptions.RepoException;
import socialnetwork.repository.memory.InMemoryRepository;

import java.io.*;

import java.util.Arrays;
import java.util.List;


///Aceasta clasa implementeaza sablonul de proiectare Template Method; puteti inlucui solutia propusa cu un Factori (vezi mai jos)
public abstract class AbstractFileRepository<ID, E extends Entity<ID>> extends InMemoryRepository<ID,E> {

    String users;

    public AbstractFileRepository(String fileName1, Validator<E> validator) {
        super(validator);
        this.users =fileName1;
        loadData();
    }

    public AbstractFileRepository(String fileName1) {
        super();
        this.users =fileName1;
        loadData();
    }

    /**
     * Loads all data from file into memory
     */
    private void loadData() {
        try (BufferedReader br = new BufferedReader(new FileReader(users))) {
            String linie;
            while((linie=br.readLine())!=null){
                List<String> attr=Arrays.asList(linie.split(";"));
                E e=extractEntity(attr);
                super.save(e);
            }
        } catch (IOException e) {
            throw new RepoException("Eroare la lucrul cu fisier!");
        }

        //sau cu lambda - curs 4, sem 4 si 5
//        Path path = Paths.get(fileName);
//        try {
//            List<String> lines = Files.readAllLines(path);
//            lines.forEach(linie -> {
//                E entity=extractEntity(Arrays.asList(linie.split(";")));
//                super.save(entity);
//            });
//        } catch (IOException e) {
//            e.printStackTrace();
//        }

    }

    /**
     *  extract entity  - template method design pattern
     *  creates an entity of type E having a specified list of @code attributes
     * @param attributes list of strings
     * @return an entity of type E
     */
    public abstract E extractEntity(List<String> attributes);
    ///Observatie-Sugestie: in locul metodei template extractEntity, puteti avea un factory pr crearea instantelor entity

    /**
     * create entity as string - template method design pattern
     * converts an entity of type E into a string of attributes
     * @param entity Generic type E
     * @return entity of type E as string
     */
    protected abstract String createEntityAsString(E entity);

    @Override
    public E delete(ID id) {
        E e = super.delete(id);

        if(e != null){
            eraseFile();
            super.findAll().forEach(this::writeToFile);
        }

        return e;
    }

    /**
     * erases the content of the file
     */
    private void eraseFile() {
        PrintWriter pw;
        try {
            pw = new PrintWriter(users);
            pw.close();
        } catch (FileNotFoundException e) {
            throw new RepoException("Eroare la lucrul cu fisier!");
        }
    }

    @Override
    public E save(E entity){
        E e=super.save(entity);
        if (e==null)
        {
            writeToFile(entity);
        }
        return e;

    }

    /**
     * Converts the entity to a String and writes it to the file
     * @param entity generic type E
     */
    protected void writeToFile(E entity){
        try (BufferedWriter bW = new BufferedWriter(new FileWriter(users,true))) {
            bW.write(createEntityAsString(entity));
            bW.newLine();
        } catch (IOException e) {
            throw new RepoException("Eroare la lucrul cu fisier!");
        }
    }
}

