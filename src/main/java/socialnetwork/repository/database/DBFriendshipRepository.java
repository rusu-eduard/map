package socialnetwork.repository.database;

import socialnetwork.domain.Prietenie;
import socialnetwork.domain.Tuple;
import socialnetwork.exceptions.DBException;
import socialnetwork.utils.Constants;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;

public class DBFriendshipRepository extends AbstractDBRepository<Tuple<Long,Long>, Prietenie> {

    public DBFriendshipRepository(String url, String username, String password) {
        super(url, username, password);
    }

    @Override
    protected PreparedStatement findOneStatement(Tuple<Long, Long> id) {
        Long id1 = id.getLeft();
        Long id2 = id.getRight();

        String dbop = "SELECT *\n" +
                "FROM \"Friendships\"\n" +
                "WHERE \"uId1\" = " + id1 + " AND \"uId2\" = " + id2;
        try {
            return this.con.prepareStatement(dbop);
        } catch (SQLException throwables) {
            throw new DBException("Failed to prepare 'findOne' statement!");
        }
    }

    @Override
    protected PreparedStatement findAllStatement() {
        String dbop = "SELECT *\n" +
                "FROM \"Friendships\"";
        try {
            return this.con.prepareStatement(dbop);
        } catch (SQLException throwables) {
            throw new DBException("Failed to preapare 'findAll' statement!");
        }
    }

    @Override
    protected Prietenie extractEntity(ResultSet resultSet) {
        try {
            long id1 = resultSet.getLong("uId1");
            long id2 = resultSet.getLong("uId2");
            LocalDateTime date = resultSet.getTimestamp("Date").toLocalDateTime();

            return new Prietenie(id1, id2, date);

        } catch (SQLException throwables) {
            throw new DBException("Failed to operate resultSet!");
        }
    }

    @Override
    protected PreparedStatement saveStatement(Prietenie entity) {
        String date = LocalDateTime.now().format(Constants.DATE_TIME_FORMATTER);
        String dbop = "INSERT INTO \"Friendships\"(\"uId1\", \"uId2\", \"Date\")\n" +
                "VALUES (" + entity.getId().getLeft() + ", "+ entity.getId().getRight() +", '"+ date +"')"+
                "returning *";
        try {
            return this.con.prepareStatement(dbop);
        } catch (SQLException throwables) {
            throw new DBException("Failed to preapare 'save' statement!");
        }
    }

    @Override
    protected PreparedStatement deleteStatement(Tuple<Long, Long> id) {
        String dbop = "DELETE FROM \"Friendships\"\n" +
                "WHERE \"uId1\" = " + id.getLeft()  + " AND \"uId2\" = " + id.getRight() + "\n" +
                "RETURNING *";
        try {
            return this.con.prepareStatement(dbop);
        } catch (SQLException throwables) {
            throw new DBException("Failed to preapare 'save' statement!");
        }
    }
}
