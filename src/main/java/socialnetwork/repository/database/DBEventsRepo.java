package socialnetwork.repository.database;

import socialnetwork.domain.Event;
import socialnetwork.domain.EventStatus;
import socialnetwork.domain.Message;
import socialnetwork.exceptions.DBException;
import socialnetwork.exceptions.RepoException;

import java.sql.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DBEventsRepo {
    Connection con;

    public DBEventsRepo(String url, String username, String password) {
        try {
            this.con = DriverManager.getConnection(url, username, password);
            System.out.println("Connected successfully");
        } catch (SQLException throwables) {
            throw new DBException("Failed to connect to database!");
        }
    }

    public void save(Event ev) {
        String dbop1 = "INSERT INTO \"Events\" (name, description, organiser, start_date, status) VALUES (?, ?, ?, ?, ?) returning event_id";
        String dbop2 = "INSERT INTO \"EventsUsers\" (event_id, user_id, notification) VALUES (?, ?, ?)";
        try {
            PreparedStatement statement1 = con.prepareStatement(dbop1);
            PreparedStatement statement2 = con.prepareStatement(dbop2);
            statement1.setString(1, ev.getName());
            statement1.setString(2, ev.getDescription());
            statement1.setLong(3,ev.getOrganizer());
            statement1.setTimestamp(4, Timestamp.valueOf(ev.getStartDate()));
            statement1.setString(5, ev.getStatus().toString());

            ResultSet set = statement1.executeQuery();
            if(set.next()){
                long id = set.getLong("event_id");
                statement2.setLong(1,id);
                ev.getParticipants().forEach((key, value) -> {
                    try {
                        statement2.setLong(2, key);
                        statement2.setBoolean(3, value);
                        statement2.execute();
                    } catch (SQLException throwables) {
                        throwables.printStackTrace();
                    }
                });
            }
        } catch (SQLException throwables) {
            throw new DBException("DB save error!");
        }
    }

    public void updateEvStatus(long id, EventStatus status){
        String dbop = "update \"Events\" set status = ? where event_id = ?";
        try {
            PreparedStatement statement = con.prepareStatement(dbop);
            statement.setString(1, status.toString());
            statement.setLong(2, id);
            statement.execute();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    public Iterable<Event> getEvents(){
        List<Event> rez = new ArrayList<>();
        String dbop1 = "Select * from \"Events\" where status LIKE ? ";
        String dbop2 ="SELECT EU.user_id, EU.notification from \"EventsUsers\" EU INNER JOIN \"Events\" E on E.event_id = EU.event_id where E.event_id = ?";
        try {
            PreparedStatement statement1 = con.prepareStatement(dbop1);
            PreparedStatement statement2 = con.prepareStatement(dbop2);
            statement1.setString(1, "COMING");
            ResultSet set1 = statement1.executeQuery();
            while(set1.next()){
                String name = set1.getString("name");
                String desc = set1.getString("description");
                Long organiser = set1.getLong("organiser");
                Long id = set1.getLong("event_id");
                String status = set1.getString("status");
                LocalDateTime date = set1.getTimestamp("start_date").toLocalDateTime();
                Map<Long, Boolean> participants = new HashMap<>();
                statement2.setLong(1, id);
                ResultSet set2 = statement2.executeQuery();
                while(set2.next()){
                    Long uId = set2.getLong("user_id");
                    Boolean notification = set2.getBoolean("notification");
                    participants.put(uId, notification);
                }

                Event ev = new Event(organiser, name, desc, date, participants, EventStatus.COMING);
                ev.setId(id);
                rez.add(ev);
            }
            return rez;
        } catch (SQLException throwables) {
            throw new RepoException("DB Error!");
        }
    }

    public void addParticipantToEv(Event ev, Long id) {
        String dbop = "INSERT INTO \"EventsUsers\" (event_id, user_id, notification) VALUES (?, ?, ?)";
        try {
            PreparedStatement statement = con.prepareStatement(dbop);
            statement.setLong(1, ev.getId());
            statement.setLong(2, id);
            statement.setBoolean(3, true);
            statement.execute();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    public Iterable<Event> getEventsForUser(Long id) {
        List<Event> rez = new ArrayList<>();
        String dbop1 = "Select * from \"Events\" E inner join \"EventsUsers\" EU on E.event_id = EU.event_id where Eu.user_id = ? and e.status LIKE ?";
        String dbop2 ="SELECT EU.user_id, EU.notification from \"EventsUsers\" EU INNER JOIN \"Events\" E on E.event_id = EU.event_id where E.event_id = ?";
        try {
            PreparedStatement statement1 = con.prepareStatement(dbop1);
            PreparedStatement statement2 = con.prepareStatement(dbop2);
            statement1.setLong(1, id);
            statement1.setString(2, "COMING");
            ResultSet set1 = statement1.executeQuery();
            while(set1.next()){
                String name = set1.getString("name");
                String desc = set1.getString("description");
                Long organiser = set1.getLong("organiser");
                Long event_id = set1.getLong("event_id");
                LocalDateTime date = set1.getTimestamp("start_date").toLocalDateTime();
                Map<Long, Boolean> participants = new HashMap<>();
                statement2.setLong(1, event_id);
                ResultSet set2 = statement2.executeQuery();
                while(set2.next()){
                    Long uId = set2.getLong("user_id");
                    Boolean notification = set2.getBoolean("notification");
                    participants.put(uId, notification);
                }

                Event ev = new Event(organiser, name, desc, date, participants, EventStatus.COMING);
                ev.setId(event_id);
                rez.add(ev);
            }
            return rez;
        } catch (SQLException throwables) {
            throw new RepoException("DB Error!");
        }
    }

    public void unnotifyUser(Long id, Long id1) {
        String dbop1 = "update \"EventsUsers\" set notification = false where event_id = ? and user_id = ?";
        try {
            PreparedStatement statement = con.prepareStatement(dbop1);
            statement.setLong(1, id);
            statement.setLong(2, id1);
            statement.execute();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    public void notifyUser(Long id, Long id1) {
        String dbop1 = "update \"EventsUsers\" set notification = true where event_id = ? and user_id = ?";
        try {
            PreparedStatement statement = con.prepareStatement(dbop1);
            statement.setLong(1, id);
            statement.setLong(2, id1);
            statement.execute();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    public void removeParticipantFromEv(Event ev, Long id) {
        String dbop = "DELETE FROM \"EventsUsers\" where event_id = ? and user_id = ? ";
        try {
            PreparedStatement statement = con.prepareStatement(dbop);
            statement.setLong(1, ev.getId());
            statement.setLong(2, id);
            statement.execute();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }
}
