package socialnetwork.repository.database;

import socialnetwork.domain.Utilizator;
import socialnetwork.exceptions.DBException;

import java.sql.*;
import java.time.LocalDateTime;

public class DBUserRepository extends AbstractDBRepository<Long,Utilizator> {

    public DBUserRepository(String url, String username, String password) {
        super(url, username, password);
    }

    @Override
    protected PreparedStatement findOneStatement(Long id) {
        String dbop ="SELECT *\n" +
                "FROM \"Users\"\n" +
                "WHERE \"uId\" = ?";
        try {
            PreparedStatement statement = this.con.prepareStatement(dbop);
            statement.setLong(1, id);
            return statement;
        } catch (SQLException throwables) {
            throw new DBException("Failed to preapare 'findOne' statement!");
        }
    }

    @Override
    protected PreparedStatement findAllStatement() {
        try {
            return this.con.prepareStatement("select *" +
                    "from \"Users\"" +
                    "order by \"uId\"");
        } catch (SQLException throwables) {
            throw new DBException("Failed to preapare 'findAll' statement!");
        }
    }

    @Override
    protected Utilizator extractEntity(ResultSet resultSet) {
        try {
            Long id = resultSet.getLong("uId");
            String firstName = resultSet.getString("uFirstName");
            String lastName = resultSet.getString("uLastName");
            String password = resultSet.getString("password");

            Utilizator utilizator = new Utilizator(firstName, lastName, password);
            utilizator.setPassword(utilizator.decryptPassword());
            utilizator.setId(id);

            return utilizator;

        } catch (SQLException throwables) {
            throw new DBException("Failed to operate resultSet!");
        }
    }

    @Override
    protected PreparedStatement saveStatement(Utilizator entity) {
        String dbop = "insert into \"Users\"(\"uFirstName\", \"uLastName\", password) VALUES (?,?,?) returning *";
        try {
            PreparedStatement statement = this.con.prepareStatement(dbop);
            statement.setString(1,entity.getFirstName());
            statement.setString(2,entity.getLastName());
            statement.setString(3, entity.getPassword());
            return statement;
        } catch (SQLException throwables) {
            throw new DBException("Failed to preapare 'save' statement!");
        }
    }

    @Override
    protected PreparedStatement deleteStatement(Long id) {
        String dbop = "DELETE FROM \"Users\"" +
                "WHERE \"uId\" = "+id+"" +
                "RETURNING *";
        try {
            return this.con.prepareStatement(dbop);
        } catch (SQLException throwables) {
            throw new DBException("Failed to preapare 'save' statement!");
        }
    }
}
