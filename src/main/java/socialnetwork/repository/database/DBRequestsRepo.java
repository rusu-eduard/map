package socialnetwork.repository.database;

import socialnetwork.domain.FriendRequest;
import socialnetwork.domain.FriendRequestStatus;
import socialnetwork.exceptions.DBException;
import socialnetwork.exceptions.RepoException;

import java.sql.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class DBRequestsRepo {
    Connection con;

    public DBRequestsRepo(String url, String username, String password) {
        try {
            this.con = DriverManager.getConnection(url, username, password);
            System.out.println("Connected successfully");
        } catch (SQLException throwables) {
            throw new DBException("Failed to connect to database!");
        }
    }

    public FriendRequest findOne(long idTo, long idFrom){
        String dbop = "SELECT * FROM \"FriendREquest\" WHERE \"toId\" = ? and \"fromId\" = ? and \"Status\" = 'PENDING'";
        try {
            PreparedStatement statement = con.prepareStatement(dbop);
            statement.setLong(1, idTo);
            statement.setLong(2,idFrom);
            ResultSet set = statement.executeQuery();
            if(set.next()){
                Long id = set.getLong("id");
                String status = set.getString("Status");
                LocalDateTime time = set.getTimestamp("date").toLocalDateTime();
                FriendRequest request = new FriendRequest(idFrom,idTo);
                request.setDate(time);
                request.setId(id);
                if(status.equals("APPROVED"))
                    request.setStatus(FriendRequestStatus.APPROVED);
                else
                    request.setStatus(FriendRequestStatus.PENDING);
                return request;
            }
            else
                return null;
        } catch (SQLException throwables) {
            throw new RepoException("DB Error!");
        }

    }

    public void save(FriendRequest fr){
        if(findOne(fr.getTo(), fr.getFrom()) == null){
            String dbop = "INSERT INTO \"FriendREquest\"(\"toId\", \"fromId\", \"Status\", date) VALUES (?,?,?,?)";
            try {
                PreparedStatement statement = con.prepareStatement(dbop);
                statement.setLong(1, fr.getTo());
                statement.setLong(2, fr.getFrom());
                statement.setString(3, "PENDING");
                statement.setTimestamp(4,Timestamp.valueOf(fr.getDate()));
                statement.execute();
            } catch (SQLException throwables) {
                throw new RepoException("DB Error!");
            }
        }
        else
            throw new RepoException("There is already a PENDING request between the 2 users");
    }

    public Iterable<FriendRequest> findAllFor(long idU){
        String dbop = "Select * from \"FriendREquest\" where \"toId\" = ?";
        try {
            PreparedStatement statement = con.prepareStatement(dbop);
            statement.setLong(1, idU);
            List<FriendRequest> requests = new ArrayList<>();
            ResultSet set = statement.executeQuery();
            while(set.next()){
                long fromId = set.getLong("fromId");
                long id = set.getLong("id");
                LocalDateTime time = set.getTimestamp("date").toLocalDateTime();
                String status = set.getString("Status");
                FriendRequest request = new FriendRequest(fromId,idU);
                request.setDate(time);
                request.setId(id);
                switch (status){
                    case "APPROVED":
                        request.setStatus(FriendRequestStatus.APPROVED);
                        break;
                    case "DECLINED":
                        request.setStatus(FriendRequestStatus.DECLINED);
                        break;
                    default:
                        request.setStatus(FriendRequestStatus.PENDING);
                        break;
                }
                requests.add(request);
            }
            return requests;
        } catch (SQLException throwables) {
            throw new RepoException("DB Error!");
        }
    }

    public Iterable<FriendRequest> findAllFromTo(long idU){
        String dbop = "Select * from \"FriendREquest\" where \"toId\" = ? or \"fromId\" = ?";
        try {
            PreparedStatement statement = con.prepareStatement(dbop);
            statement.setLong(1, idU);
            statement.setLong(2, idU);
            List<FriendRequest> requests = new ArrayList<>();
            ResultSet set = statement.executeQuery();
            while(set.next()){
                long fromId = set.getLong("fromId");
                long toId = set.getLong("toId");
                long id = set.getLong("id");
                LocalDateTime time = set.getTimestamp("date").toLocalDateTime();
                String status = set.getString("Status");
                FriendRequest request = new FriendRequest(fromId,toId);
                request.setDate(time);
                request.setId(id);
                switch (status){
                    case "APPROVED":
                        request.setStatus(FriendRequestStatus.APPROVED);
                        break;
                    case "DECLINED":
                        request.setStatus(FriendRequestStatus.DECLINED);
                        break;
                    default:
                        request.setStatus(FriendRequestStatus.PENDING);
                        break;
                }
                requests.add(request);
            }
            return requests;
        } catch (SQLException throwables) {
            throw new RepoException("DB Error!");
        }
    }

    public void updateRequest(FriendRequest request, FriendRequestStatus status) {
        String dbop = "UPDATE \"FriendREquest\" set \"Status\" = ? where \"Status\" = 'PENDING' and \"toId\" = ? and \"fromId\" = ?";
        try {
            PreparedStatement statement = con.prepareStatement(dbop);
            if(status.equals(FriendRequestStatus.APPROVED))
                statement.setString(1,"APPROVED");
            else
                statement.setString(1,"DECLINED");
            statement.setLong(2,request.getTo());
            statement.setLong(3,request.getFrom());
            statement.execute();
        } catch (SQLException throwable) {
            throwable.printStackTrace();
        }
    }
}
