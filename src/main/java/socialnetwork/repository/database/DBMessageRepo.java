package socialnetwork.repository.database;

import socialnetwork.domain.*;
import socialnetwork.exceptions.DBException;
import socialnetwork.exceptions.RepoException;

import java.sql.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class DBMessageRepo {
    Connection con;

    public DBMessageRepo(String url, String username, String password) {
        try {
            this.con = DriverManager.getConnection(url, username, password);
            System.out.println("Connected successfully");
        } catch (SQLException throwables) {
            throw new DBException("Failed to connect to database!");
        }
    }

    public void save(Message m){
        String dbop1 = "INSERT INTO \"Messages\"(fromuser, msg, date, reply, \"toUser\", \"toGroup\") VALUES (?,?,?,?,?,?)";
        try {
            PreparedStatement statement1 = con.prepareStatement(dbop1);
            statement1.setLong(1,m.getFrom());
            statement1.setString(2,m.getMessage());
            statement1.setTimestamp(3, Timestamp.valueOf(m.getDate()));
            if(m.getReply()!=null)
                statement1.setLong(4,m.getReply());
            else
                statement1.setNull(4, Types.BIGINT);
            if(m.getToUser() != null){
                statement1.setLong(5, m.getToUser());
                statement1.setNull(6, Types.BIGINT);
            }
            else {
                statement1.setNull(5, Types.BIGINT);
                statement1.setLong(6, m.getToGroup());
            }
            statement1.execute();
        } catch (SQLException throwables) {
            throw new RepoException("BD Error!");
        }
    }

    public Iterable<Message> getMsgBetweenUsers(long id1, long id2){
        String dbop1 = "select * from \"Messages\" where fromuser = ? and \"toUser\" = ? or fromuser = ? and \"toUser\" = ?";
        List<Message> msgs = new ArrayList<>();
        try {
            PreparedStatement statement1 = con.prepareStatement(dbop1);
            statement1.setLong(1, id1);
            statement1.setLong(2, id2);
            statement1.setLong(3, id2);
            statement1.setLong(4, id1);
            ResultSet set = statement1.executeQuery();
            while(set.next()){
                Long id = set.getLong("id");
                Long from = set.getLong("fromuser");
                String msg = set.getString("msg");
                LocalDateTime date = set.getTimestamp("date").toLocalDateTime();
                Long reply = set.getLong("reply");
                Long toUser = set.getLong("toUser");
                Long toGroup = set.getLong("toGroup");
                Message m = new Message(from, msg, date, reply, toUser, toGroup);
                m.setId(id);
                msgs.add(m);
            }
            return msgs;
        } catch (SQLException throwables) {
            throw new DBException("Failed to execute query");
        }
    }

    public Iterable<Message> getMsgBetweenUsers(long id1, long id2, int pageNumber, int pageSize){
        String dbop1 = "select * from \"Messages\" where fromuser = ? and \"toUser\" = ? or fromuser = ? and \"toUser\" = ? limit ? offset ?";
        List<Message> msgs = new ArrayList<>();
        try {
            PreparedStatement statement1 = con.prepareStatement(dbop1);
            statement1.setLong(1, id1);
            statement1.setLong(2, id2);
            statement1.setLong(3, id2);
            statement1.setLong(4, id1);
            statement1.setInt(5, pageSize);
            statement1.setInt(6, (pageNumber-1)*pageSize);
            ResultSet set = statement1.executeQuery();
            while(set.next()){
                Long id = set.getLong("id");
                Long from = set.getLong("fromuser");
                String msg = set.getString("msg");
                LocalDateTime date = set.getTimestamp("date").toLocalDateTime();
                Long reply = set.getLong("reply");
                Long toUser = set.getLong("toUser");
                Long toGroup = set.getLong("toGroup");
                Message m = new Message(from, msg, date, reply, toUser, toGroup);
                m.setId(id);
                msgs.add(m);
            }
            return msgs;
        } catch (SQLException throwables) {
            throw new DBException("Failed to execute query");
        }
    }

    public Iterable<Message> getMessagesForGroup(long gId){
        List<Message> msgs = new ArrayList<>();
        String dbop = "select * from \"Messages\" where \"toGroup\" = ?";
        try {
            PreparedStatement statement = con.prepareStatement(dbop);
            statement.setLong(1, gId);
            ResultSet set = statement.executeQuery();
            while(set.next()){
                Long id = set.getLong("id");
                Long from = set.getLong("fromuser");
                String msg = set.getString("msg");
                LocalDateTime date = set.getTimestamp("date").toLocalDateTime();
                Long reply = set.getLong("reply");
                Long toUser = set.getLong("toUser");
                Long toGroup = set.getLong("toGroup");
                Message m = new Message(from, msg, date, reply, toUser, toGroup);
                m.setId(id);
                msgs.add(m);
            }
            return msgs;
        } catch (SQLException throwables) {
            throw new DBException("DB exception");
        }
    }

    public Iterable<Message> getMessagesForGroup(long gId, int pageNumber, int pageSize){
        List<Message> msgs = new ArrayList<>();
        String dbop = "select * from \"Messages\" where \"toGroup\" = ? limit ? offset ?";
        try {
            PreparedStatement statement = con.prepareStatement(dbop);
            statement.setLong(1, gId);
            statement.setInt(2, pageSize);
            statement.setInt(3 ,(pageNumber-1)*pageSize);
            ResultSet set = statement.executeQuery();
            while(set.next()){
                Long id = set.getLong("id");
                Long from = set.getLong("fromuser");
                String msg = set.getString("msg");
                LocalDateTime date = set.getTimestamp("date").toLocalDateTime();
                Long reply = set.getLong("reply");
                Long toUser = set.getLong("toUser");
                Long toGroup = set.getLong("toGroup");
                Message m = new Message(from, msg, date, reply, toUser, toGroup);
                m.setId(id);
                msgs.add(m);
            }
            return msgs;
        } catch (SQLException throwables) {
            throw new DBException("DB exception");
        }
    }

    public Message getMessage(Long mId){
        String dbop = "select * from \"Messages\" where id = ?";
        try {
            PreparedStatement statement = con.prepareStatement(dbop);
            statement.setLong(1, mId);
            ResultSet set = statement.executeQuery();
            if(set.next()){
                Long id = set.getLong("id");
                Long from = set.getLong("fromuser");
                String msg = set.getString("msg");
                LocalDateTime date = set.getTimestamp("date").toLocalDateTime();
                Long reply = set.getLong("reply");
                Long toUser = set.getLong("toUser");
                Long toGroup = set.getLong("toGroup");
                Message m = new Message(from, msg, date, reply, toUser, toGroup);
                m.setId(id);
                return m;
            }
            else
                return null;
        } catch (SQLException throwables) {
            throw new DBException("DB exception");
        }
    }

    public Iterable<Long> getUsersWithConv(Long id){
        List<Long> users = new ArrayList<>();
        String dbop = "select fromuser, \"toUser\" from \"Messages\" where \"toGroup\" is null and fromuser = ? or \"toUser\" = ? order by date desc";
        try {
            PreparedStatement statement = con.prepareStatement(dbop);
            statement.setLong(1, id);
            statement.setLong(2, id);
            ResultSet set = statement.executeQuery();
            while(set.next()){
                Long fromUser = set.getLong("fromuser");
                Long toUser = set.getLong("toUser");
                if(!toUser.equals(id) && !users.contains(toUser))
                    users.add(toUser);
                if(!fromUser.equals(id) && !users.contains(fromUser))
                    users.add(fromUser);
            }
            return users;
        } catch (SQLException throwables) {
            throw new DBException("DB exception");
        }
    }

    public void saveGroup(Group gr){
        String dbop = "Insert Into \"Group\" (name) values (?) returning id";
        String dbop2 = "INSERT INTO \"GroupUsers\" (u_id, g_id) values (?, ?)";
        try {
            PreparedStatement statement = con.prepareStatement(dbop);
            PreparedStatement statement2 = con.prepareStatement(dbop2);
            statement.setString(1, gr.getName());
            ResultSet set = statement.executeQuery();
            if(set.next()){
                Long g_id = set.getLong("id");
                gr.getMembers().forEach(x->{
                    try {
                        statement2.setLong(1, x);
                        statement2.setLong(2, g_id);
                        statement2.execute();
                    } catch (SQLException throwables) {
                        throwables.printStackTrace();
                    }
                });
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    public Iterable<Group> getGroupsWithUser(Long id){
        List<Group> groups = new ArrayList<>();
        String dbop = "SELECT G.id, G.name from \"Group\" G INNER JOIN \"GroupUsers\" GU on G.id = GU.g_id where GU.u_id = ?";
        String dbop2 = "SELECT GU.u_id from \"GroupUsers\" GU INNER JOIN \"Group\" G on G.id = GU.g_id where G.id = ?";
        try {
            PreparedStatement statement = con.prepareStatement(dbop);
            PreparedStatement statement2 = con.prepareStatement(dbop2);
            statement.setLong(1, id);
            ResultSet set = statement.executeQuery();
            while(set.next()){
                Long g_id = set.getLong("id");
                String g_name = set.getString("name");
                statement2.setLong(1, g_id);
                List<Long> members = new ArrayList<>();
                ResultSet set2 = statement2.executeQuery();
                while (set2.next()){
                    Long u_id = set2.getLong("u_id");
                    members.add(u_id);
                }
                Group g = new Group(g_name, members);
                g.setId(g_id);
                groups.add(g);
            }
            return groups;
        } catch (SQLException throwables) {
            throw new DBException("DB exception");
        }
    }

    public Group getGroup(Long gId){
        String dbop = "SELECT G.id, G.name from \"Group\" G  where G.id = ?";
        String dbop2 = "SELECT GU.u_id from \"GroupUsers\" GU INNER JOIN \"Group\" G on G.id = GU.g_id where G.id = ?";
        try {
            PreparedStatement statement = con.prepareStatement(dbop);
            PreparedStatement statement2 = con.prepareStatement(dbop2);
            statement.setLong(1, gId);
            ResultSet set = statement.executeQuery();
            if(set.next()){
                Long g_id = set.getLong("id");
                String g_name = set.getString("name");
                statement2.setLong(1, g_id);
                List<Long> members = new ArrayList<>();
                ResultSet set2 = statement2.executeQuery();
                while (set2.next()){
                    Long u_id = set2.getLong("u_id");
                    members.add(u_id);
                }
                Group g = new Group(g_name, members);
                g.setId(g_id);
                return g;
            }
            return null;
        } catch (SQLException throwables) {
            throw new DBException("DB exception");
        }
    }

    public Iterable<Utilizator> getAllExceptOne(Long id){
        List<Utilizator> users = new ArrayList<>();
        String dbop = "select * from \"Users\" where \"uId\" <> ? ";
        try {
            PreparedStatement statement = con.prepareStatement(dbop);
            statement.setLong(1, id);
            ResultSet set = statement.executeQuery();
            while(set.next()){
                String firstName = set.getString("uFirstName");
                String lastName = set.getString("uLastName");
                Long uId = set.getLong("uId");
                Utilizator user = new Utilizator(firstName, lastName);
                user.setId(uId);
                users.add(user);
            }
            return users;
        } catch (SQLException throwables) {
            throw new DBException("DB exception");
        }
    }

    public Iterable<Message> getMessagesTo(Long id){
        List<Message> messages = new ArrayList<>();
        String dbop = "SELECT * from \"Messages\" where \"toUser\" = ?";
        try {
            PreparedStatement statement = con.prepareStatement(dbop);
            statement.setLong(1, id);
            ResultSet set = statement.executeQuery();
            while(set.next()){
                Long mid = set.getLong("id");
                Long from = set.getLong("fromuser");
                String msg = set.getString("msg");
                LocalDateTime date = set.getTimestamp("date").toLocalDateTime();
                Message m = new Message(from, msg, date, id, null);
                m.setId(mid);
                messages.add(m);
            }
            return messages;
        } catch (SQLException throwables) {
            throw new DBException("DB exception");
        }
    }

    public Iterable<DTOUtilizator> getFriendsForUser(Long id){
        List<DTOUtilizator> friends = new ArrayList<>();
        String dbop = "select u.\"uId\", u.\"uFirstName\", u.\"uLastName\", f.\"Date\" from \"Users\" U\n" +
                "inner join \"Friendships\" F on U.\"uId\" = F.\"uId1\" and ? = F.\"uId2\" or ? = F.\"uId1\" and U.\"uId\"  = F.\"uId2\"";
        try {
            PreparedStatement statement = con.prepareStatement(dbop);
            statement.setLong(1, id);
            statement.setLong(2, id);
            ResultSet set = statement.executeQuery();
            while(set.next()){
                Long uId = set.getLong("uId");
                String firstName = set.getString("uFirstName");
                String lastName = set.getString("uLastName");
                LocalDateTime date = set.getTimestamp("Date").toLocalDateTime();
                DTOUtilizator friend = new DTOUtilizator(uId, firstName, lastName, date);
                friends.add(friend);
            }
            return friends;
        } catch (SQLException throwables) {
            throw new DBException("DB exception");
        }
    }

    public Iterable<DTOUtilizator> getFriendsForUser(Long id, int pageNumber, int pageSize, String nume){
        List<DTOUtilizator> friends = new ArrayList<>();
        String dbop = "select u.\"uId\", u.\"uFirstName\", u.\"uLastName\", f.\"Date\" from \"Users\" U\n" +
                "inner join \"Friendships\" F on U.\"uId\" = F.\"uId1\" and ? = F.\"uId2\" or ? = F.\"uId1\" and U.\"uId\"  = F.\"uId2\" " +
                "where u.\"uLastName\" LIKE ? " +
                "limit ? offset ?";
        try {
            PreparedStatement statement = con.prepareStatement(dbop);
            statement.setLong(1, id);
            statement.setLong(2, id);
            statement.setInt(4, pageSize);
            statement.setInt(5, (pageNumber-1)*pageSize);
            statement.setString(3, nume+"%");
            ResultSet set = statement.executeQuery();
            while(set.next()){
                Long uId = set.getLong("uId");
                String firstName = set.getString("uFirstName");
                String lastName = set.getString("uLastName");
                LocalDateTime date = set.getTimestamp("Date").toLocalDateTime();
                DTOUtilizator friend = new DTOUtilizator(uId, firstName, lastName, date);
                friends.add(friend);
            }
            return friends;
        } catch (SQLException throwables) {
            throw new DBException("DB exception");
        }
    }

    public Iterable<Utilizator> getPossibleFriends(Long id, int pageNumber, int pageSize, String name) {
        List<Utilizator> users = new ArrayList<>();
        String dbop = "select * from \"Users\"\n" +
                "where(not (exists(select * from \"Friendships\" where \"uId1\" = \"Users\".\"uId\" and \"uId2\" = ? or \"uId1\" = ? and \"uId2\" = \"Users\".\"uId\" )) and \"uId\" <> ? and \"uLastName\" LIKE ?)" +
                "limit ? offset ?";
        try {
            PreparedStatement statement = con.prepareStatement(dbop);
            statement.setLong(1, id);
            statement.setLong(2, id);
            statement.setLong(3, id);
            statement.setString(4, name+"%");
            statement.setInt(5, pageSize);
            statement.setInt(6, (pageNumber-1)*pageSize);
            ResultSet set = statement.executeQuery();
            while(set.next()){
                Long uId = set.getLong("uId");
                String lastName = set.getString("uLastName");
                String firstName = set.getString("uFirstName");
                Utilizator user = new Utilizator(firstName, lastName);
                user.setId(uId);
                users.add(user);
            }
            return users;
        } catch (SQLException throwables) {
            throw new DBException("DB exception");
        }
    }

    public Iterable<FriendRequest> getAllFromTo(long id, int pageNumber, int pageSize) {
        String dbop = "Select * from \"FriendREquest\" where \"toId\" = ? or \"fromId\" = ? limit ? offset ?";
        try {
            PreparedStatement statement = con.prepareStatement(dbop);
            statement.setLong(1, id);
            statement.setLong(2, id);
            statement.setInt(3, pageSize);
            statement.setInt(4, (pageNumber-1)*pageSize);
            List<FriendRequest> requests = new ArrayList<>();
            ResultSet set = statement.executeQuery();
            while(set.next()){
                long fromId = set.getLong("fromId");
                long toId = set.getLong("toId");
                long uid = set.getLong("id");
                LocalDateTime time = set.getTimestamp("date").toLocalDateTime();
                String status = set.getString("Status");
                FriendRequest request = new FriendRequest(fromId,toId);
                request.setDate(time);
                request.setId(uid);
                switch (status){
                    case "APPROVED":
                        request.setStatus(FriendRequestStatus.APPROVED);
                        break;
                    case "DECLINED":
                        request.setStatus(FriendRequestStatus.DECLINED);
                        break;
                    default:
                        request.setStatus(FriendRequestStatus.PENDING);
                        break;
                }
                requests.add(request);
            }
            return requests;
        } catch (SQLException throwables) {
            throw new RepoException("DB Error!");
        }
    }
}
