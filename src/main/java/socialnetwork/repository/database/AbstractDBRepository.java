package socialnetwork.repository.database;

import socialnetwork.domain.Entity;
import socialnetwork.exceptions.DBException;
import socialnetwork.repository.Repository;

import java.sql.*;
import java.util.HashSet;
import java.util.Set;

public abstract class AbstractDBRepository<ID, E extends Entity<ID>> implements Repository<ID,E> {
    Connection con;

    public AbstractDBRepository(String url, String username, String password) {
        try {
            this.con = DriverManager.getConnection(url, username, password);
            System.out.println("Connected successfully");
        } catch (SQLException throwables) {
            throw new DBException("Failed to connect to database!");
        }
    }

    @Override
    public E findOne(ID id) {
        PreparedStatement statement = findOneStatement(id);
        try {
            ResultSet resultSet = statement.executeQuery();
            if(resultSet.next()){
                return extractEntity(resultSet);
            }
            return null;
        } catch (SQLException throwables) {
            throw new DBException("Failed to execute 'findOne' statement!");
        }
    }

    protected abstract PreparedStatement findOneStatement(ID id);


    @Override
    public Iterable<E> findAll() {
        Set<E> entities = new HashSet<>();
        PreparedStatement statement = findAllStatement();
        try {
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()){
                E entity = extractEntity(resultSet);
                entities.add(entity);
            }
            return entities;
        } catch (SQLException throwables) {
            throw new DBException("Failed to execute 'findAll' statement!");
        }
    }

    protected abstract PreparedStatement findAllStatement();

    protected abstract E extractEntity(ResultSet resultSet);

    @Override
    public E save(E entity) {
        try {
            ResultSet set = saveStatement(entity).executeQuery();
            if(set.next()){
                return extractEntity(set);
            }
            return null;
        } catch (SQLException throwables) {
            throw new DBException("Failed to execute 'save' statement!");
        }
    }

    protected abstract PreparedStatement saveStatement(E entity);

    @Override
    public E delete(ID id) {
        PreparedStatement statement = deleteStatement(id);
        try {
            ResultSet resultSet = statement.executeQuery();
            if(resultSet.next()){
                return extractEntity(resultSet);
            }
            return null;
        } catch (SQLException throwables) {
            throw new DBException("Failed to execute 'delete' statement!");
        }
    }

    protected abstract PreparedStatement deleteStatement(ID id);

    @Override
    public E update(E entity) {
        return null;
    }
}
