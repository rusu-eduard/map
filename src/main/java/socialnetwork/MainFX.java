package socialnetwork;

import controller.LogInController;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.stage.Stage;
import javafx.scene.Scene;

import socialnetwork.config.ApplicationContext;
import socialnetwork.domain.Prietenie;
import socialnetwork.domain.Tuple;
import socialnetwork.domain.Utilizator;
import socialnetwork.repository.Repository;
import socialnetwork.repository.database.*;
import socialnetwork.service.ServiceWithUser;
import socialnetwork.service.UtilizatorService;

import java.io.IOException;

public class MainFX extends Application {

    final String url = ApplicationContext.getPROPERTIES().getProperty("database.socialnetwork.url");
    final String username = ApplicationContext.getPROPERTIES().getProperty("database.socialnetwork.username");
    final String password = ApplicationContext.getPROPERTIES().getProperty("database.socialnetwork.password");
    ServiceWithUser serviceUtizilator;
    UtilizatorService service;
    Repository<Tuple<Long,Long>, Prietenie> friendshipDBRepository;
    Repository<Long, Utilizator> userDBRepository;
    DBMessageRepo messageDBRepo;
    DBRequestsRepo requestsRepo;
    DBEventsRepo eventsRepo;

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        friendshipDBRepository = new DBFriendshipRepository(url, username, password);
        userDBRepository = new DBUserRepository(url,username,password);
        messageDBRepo = new DBMessageRepo(url,username,password);
        requestsRepo = new DBRequestsRepo(url,username,password);
        eventsRepo = new DBEventsRepo(url, username, password);

        service = new UtilizatorService(userDBRepository, friendshipDBRepository, messageDBRepo, requestsRepo, eventsRepo);

        initView(primaryStage);
        primaryStage.setTitle("Social Network");
        primaryStage.show();
    }

    private void initView(Stage primaryStage) throws IOException {

        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/views/logInView.fxml"));
        Parent root = loader.load();
        primaryStage.setScene(new Scene(root));

        LogInController logInController = loader.getController();
        logInController.setLogInControllerService(service);
    }
}
