package socialnetwork.utils.events;

import socialnetwork.domain.DTOUtilizator;
import socialnetwork.domain.Prietenie;

public class PrietenieChangeEvent implements Event {
    private final ChangeEventType type;

    public PrietenieChangeEvent(ChangeEventType type) {
        this.type = type;
    }

    public ChangeEventType getType() {
        return type;
    }

}
