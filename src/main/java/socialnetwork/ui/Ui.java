//package socialnetwork.ui;
//
//import socialnetwork.domain.Prietenie;
//import socialnetwork.domain.Utilizator;
//import socialnetwork.domain.validators.ValidationException;
//import socialnetwork.exceptions.DBException;
//import socialnetwork.exceptions.FriendshipException;
//import socialnetwork.exceptions.RepoException;
//import socialnetwork.exceptions.ServiceException;
//import socialnetwork.service.UtilizatorService;
//
//import java.io.BufferedReader;
//import java.io.IOException;
//import java.io.InputStreamReader;
//
//public class Ui {
//
//    UtilizatorService service;
//    BufferedReader reader;
//
//    public Ui(UtilizatorService service) {
//        this.service = service;
//        this.reader = new BufferedReader(new InputStreamReader(System.in));
//    }
//
//    public void run(){
//        String cmd;
//        boolean running = true;
//        while(running){
//            System.out.println("Available commands:\n\tAdd user.\n\tRemove user.\n\tSend request\n\tAccept request\n\tRemove request\n\tRemove friendship\n\tGet friendships.\n\tGet users.\n\tGet communities\n\tGet longest community\n\tGet friends of\n\tGet month friends\n\tSend message\n\tReply all\n\tReply one\n\tGet between\n\tExit.");
//            System.out.println("Insert command: ");
//            try {
//                cmd = reader.readLine();
//                switch (cmd){
//                    case "Remove friendship":
//                        UiRemoveFriendship();
//                        break;
//                    case "Accept request":
//                        UiAcceptRequest();
//                        break;
//                    case "Remove request":
//                        UiRemoveRequest();
//                        break;
//                    case "Add user":
//                        //UiAdaugaUtilizator();
//                        break;
//                    case "Send request":
//                        UiSendRequest();
//                        break;
//                    case "Remove user":
//                        UiRemoveUser();
//                        break;
//                    case "Send message":
//                        UiSendMessage();
//                        break;
//                    case "Reply all":
//                        UiReplyAll();
//                        break;
//                    case "Get users":
//                        UiGetUsers();
//                        break;
//                    case "Get between":
//                        UiGetBetween();
//                        break;
//                    case "Get month friends":
//                        UiGetMonthFriends();
//                        break;
//                    case "Reply one":
//                        UiReplyOne();
//                        break;
//                    case "Exit":
//                        running=false;
//                        break;
//                    case "Get friends of":
//                        UiGetFriendsOf();
//                        break;
//                    case "Get friendships":
//                        UiGetFriendships();
//                        break;
//                    case "Get communities":
//                        UiGetCommunities();
//                        break;
//                    case "Get longest community":
//                        UiGetLongestCommunity();
//                        break;
//                    default:
//                        System.out.println("Unexpected value: " + cmd);
//                        break;
//                }
//            } catch (IOException | NumberFormatException e) {
//                e.printStackTrace();
//            }
//            catch (ValidationException | FriendshipException | RepoException | ServiceException | DBException err){
//                System.out.println(err.getMessage());
//            }
//        }
//    }
//
//    private void UiRemoveRequest() throws IOException {
//        System.out.println("User: ");
//        long user = Long.parseLong(reader.readLine());
//        System.out.println("Friend requests for " + user + " are: ");
//        service.getRequestsFor(user).forEach(System.out::println);
//        System.out.println("Decline request form: ");
//        long from = Long.parseLong(reader.readLine());
//        service.deleteRequest(from, user);
//        System.out.println("Friend request removed!");
//    }
//
//    private void UiAcceptRequest() throws IOException {
//        System.out.println("User: ");
//        long user = Long.parseLong(reader.readLine());
//        System.out.println("Friend requests for " + user + " are: ");
//        service.getRequestsFor(user).forEach(System.out::println);
//        System.out.println("Accept request form: ");
//        long from = Long.parseLong(reader.readLine());
//        service.acceptRequest(from, user);
//        System.out.println("Friend request accepted!");
//    }
//
//    private void UiRemoveFriendship() throws IOException {
//        System.out.println("User 1: ");
//        Long id1 = Long.parseLong(reader.readLine());
//        System.out.println("User 2: ");
//        Long id2 = Long.parseLong(reader.readLine());
//        service.removeFriendship(id1,id2);
//        System.out.println("Friendship removed successfully!");
//    }
//
//    private void UiSendRequest() throws IOException {
//        System.out.println("From :");
//        long idFrom = Long.parseLong(reader.readLine());
//        System.out.println("To: ");
//        long idTo = Long.parseLong(reader.readLine());
//        service.sendRequest(idFrom, idTo);
//        System.out.println("Friend request sent!");
//    }
//
//    private void UiGetBetween() throws IOException {
//        System.out.println("User 1: ");
//        Long id1 = Long.parseLong(reader.readLine());
//        System.out.println("User 2: ");
//        Long id2 = Long.parseLong(reader.readLine());
//        service.getMsgBetween(id1, id2).forEach(System.out::println);
//    }
//
//    private void UiReplyOne() throws IOException {
//        System.out.println("User: ");
//        Long uId1 = Long.parseLong(reader.readLine());
//        System.out.println("Message you want to reply to: ");
//        Long msgId = Long.parseLong(reader.readLine());
//        System.out.println("User you want to reply to: ");
//        Long uId2 = Long.parseLong(reader.readLine());
//        System.out.println("Message: ");
//        String msg = reader.readLine();
//        service.replyOneMessage(msgId, msg, uId1, uId2);
//        System.out.println("Replied");
//    }
//
//    private void UiReplyAll() throws IOException {
//        System.out.println("User: ");
//        Long uId = Long.parseLong(reader.readLine());
//        System.out.println("Message you want to reply to: ");
//        Long mId = Long.parseLong(reader.readLine());
//        System.out.println("Message: ");
//        String msg = reader.readLine();
//        service.replyAllMessage(mId, msg, uId);
//        System.out.println("Replied");
//    }
//
//    private void UiSendMessage() throws IOException {
//        System.out.println("From: ");
//        String idFrom = reader.readLine();
//        System.out.println("To: ");
//        String to = reader.readLine();
//        System.out.println("Message: ");
//        String message = reader.readLine();
//        service.sendMessage(idFrom, message, to);
//        System.out.println("Message sent!");
//    }
//
//    private void UiGetMonthFriends() throws IOException {
//        System.out.println("Id of user: ");
//        long id = Long.parseLong(reader.readLine());
//        System.out.println("Month: ");
//        int month = Integer.parseInt(reader.readLine());
//        service.getDTOFriends(id,month,true).forEach(System.out::println);
//    }
//
//    private void UiGetFriendsOf() throws IOException {
//        System.out.println("Id of user: ");
//        long id = Long.parseLong(reader.readLine());
//        service.getDTOFriends(id,0,false).forEach(System.out::println);
//    }
//
//    private void UiGetLongestCommunity() {
//        Iterable<Utilizator> users = service.getLongestCommunity();
//        users.forEach(System.out::println);
//    }
//
//    private void UiGetCommunities() {
//        long count = service.getCommunities();
//        System.out.println("Number of social communities is: " + count );
//    }
//
//    private void UiGetFriendships() {
//        service.getAllFriendships().forEach(System.out::println);
//    }
//
//    private void UiRemoveUser()throws IOException {
//        System.out.println("ID of the user you want to delete: ");
//        Long id = Long.parseLong(reader.readLine());
//        Utilizator user = service.removeUser(id);
//        if (user!=null){
//            System.out.println("Removed user:\n " + user.toString());
//        }else
//            System.out.println("There was no user with id: " + id);
//    }
//
//    private void UiGetUsers() {
//        service.getAll().forEach(System.out::println);
//    }
//
////    private void UiAdaugaUtilizator()throws IOException {
////        System.out.println("First name: ");
////        String name = reader.readLine();
////        System.out.println("Second name: ");
////        String surname = reader.readLine();
////
////        Utilizator user = service.save(name, surname);
////        if(user != null){
////            System.out.println("User already exists: " +  user.toString());
////        }else
////            System.out.println("User added successfully");
////    }
//
//}
