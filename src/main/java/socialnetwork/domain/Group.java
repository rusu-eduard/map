package socialnetwork.domain;

import java.util.List;

public class Group extends Entity<Long>{
    private final String name;
    private final List<Long> members;

    public Group(String name, List<Long> members) {
        this.name = name;
        this.members = members;
    }

    public String getName() {
        return name;
    }

    public List<Long> getMembers() {
        return members;
    }
}
