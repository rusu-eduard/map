package socialnetwork.domain;
import java.time.LocalDateTime;
import socialnetwork.utils.Constants;


public class Prietenie extends Entity<Tuple<Long,Long>> {

    LocalDateTime date;

    public Prietenie(long id1, long id2) {
        this.setId(new Tuple<>(id1, id2));
        date = LocalDateTime.now();
    }

    public Prietenie(long id1, long id2, LocalDateTime data) {
        this.setId(new Tuple<>(id1, id2));
        date = data;
    }

    /**
     *
     * @return the date when the friendship was created
     */
    public LocalDateTime getDate() {
        return date;
    }

    @Override
    public String toString() {
        return "Prietenie{" +
                "ID = " + this.getId() +
                "; date = " + date.format(Constants.DATE_TIME_FORMATTER) +
                "}";
    }
}
