package socialnetwork.domain;

import java.util.Objects;


/**
 * Define a Tuple o generic type entities
 * @param <E1> - tuple first entity type
 * @param <E2> - tuple second entity type
 */
public class Tuple<E1, E2> {
    private E1 e1;
    private E2 e2;

    public Tuple(E1 e1, E2 e2) {
        this.e1 = e1;
        this.e2 = e2;
    }

    /**
     *
     * @return Left element of the Tuple
     */
    public E1 getLeft() {
        return e1;
    }

    /**
     * c
     * @param e1 of generic type
     */
    public void setLeft(E1 e1) {
        this.e1 = e1;
    }

    /**
     *
     * @return Right element of the Tuple
     */
    public E2 getRight() {
        return e2;
    }

    /**
     * Sets the right element of the Tuple to e2
     * @param e2 of generic type
     */
    public void setRight(E2 e2) {
        this.e2 = e2;
    }

    @Override
    public String toString() {
        return "" + e1 + ";" + e2;
    }

    @Override
    public boolean equals(Object obj) {
        return this.e1.equals(((Tuple) obj).e1) && this.e2.equals(((Tuple) obj).e2) || this.e1.equals(((Tuple) obj).e2) && this.e2.equals(((Tuple) obj).e1);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(e1.hashCode() + e2.hashCode());
    }
}