package socialnetwork.domain;

import socialnetwork.utils.Constants;
import java.time.LocalDateTime;

public class MessageDTO extends Entity<Long> {
    private final String from;
    private final String msg;
    private final LocalDateTime date;
    private final String to;
    private final String outputMessage;

    public MessageDTO(String from, String msg, LocalDateTime date, String to, long id) {
        this.from = from;
        this.msg = msg;
        this.date = date;
        this.to = to;
        this.setId(id);
        this.outputMessage = "From : " + from + "\nDate: " + date.format(Constants.DATE_TIME_FORMATTER) + "\nMessage: " + msg;
    }

    public MessageDTO(String lastName, String message, String replyToMsg, LocalDateTime date, String lastName1, Long id) {
        this.from = lastName;
        this.msg = message;
        this.date = date;
        this.to = lastName1;
        this.setId(id);
        this.outputMessage = "From : " + from + "\nDate: " + date.format(Constants.DATE_TIME_FORMATTER) + "\nMessage: " + msg + "\nReplyed to: " + replyToMsg;
    }

    public String getFrom() {
        return from;
    }

    public String getMsg() {
        return msg;
    }

    public String getTo() {
        return to;
    }

    public LocalDateTime getDate() {
        return date;
    }

    public String getOutputMessage() {
        return outputMessage;
    }

    @Override
    public String toString() {
        return "From: " + from +
                ", msg: '" + msg + '\'' +
                ", to: " + to +
                ", date: " + date.format(Constants.DATE_TIME_FORMATTER) +
                '}';
    }
}
