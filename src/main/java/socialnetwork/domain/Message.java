package socialnetwork.domain;

import java.time.LocalDateTime;
import java.util.List;

public class Message extends Entity<Long> {

    private final Long from;
    private final Long toUser;
    private final Long toGroup;
    private final String message;
    private final LocalDateTime date;
    private final Long reply;

    public Message(Long from, String message, Long toUser, Long toGroup) {
        this.from = from;
        this.toUser = toUser;
        this.toGroup = toGroup;
        this.message = message;
        this.date = LocalDateTime.now();
        this.reply = null;
    }

    public Message(Long from, String message, Long reply, Long toUser, Long toGroup) {
        this.from = from;
        this.toGroup = toGroup;
        this.toUser = toUser;
        this.message = message;
        this.date = LocalDateTime.now();
        this.reply = reply;
    }

    public Message(Long from, String message, LocalDateTime date, Long toUser, Long toGroup) {
        this.from = from;
        this.toUser = toUser;
        this.toGroup = toGroup;
        this.message = message;
        this.date = date;
        this.reply = null;
    }

    public Message(Long from, String message, LocalDateTime date, long reply, Long toUser, Long toGroup) {
        this.from = from;
        this.toUser = toUser;
        this.toGroup = toGroup;
        this.message = message;
        this.date = date;
        this.reply = reply;
    }

    public Long getFrom() {
        return from;
    }

    public Long getToUser() {
        return toUser;
    }

    public Long getToGroup() {
        return toGroup;
    }

    public String getMessage() {
        return message;
    }

    public LocalDateTime getDate() {
        return date;
    }

    public Long getReply() {
        return reply;
    }



    @Override
    public String toString() {
        return "Message{" +
                "from=" + from +
                ", message='" + message + '\'' +
                ", date=" + date +
                '}';
    }
}
