package socialnetwork.domain.validators;

import socialnetwork.domain.Utilizator;

public class UtilizatorValidator implements Validator<Utilizator> {
    @Override
    public void validate(Utilizator entity) throws ValidationException {
        String err = "";
        if(entity.getId() < 0){
            err+="Invalid ID! Must be a positive number!\n";
        }
        if(entity.getFirstName().isEmpty()){
            err+="First name cannot be empty!\n";
        }
        if(entity.getLastName().isEmpty()){
            err+="Last name cannot be empty!\n";
        }
        if(!err.isEmpty()){
            throw new ValidationException(err);
        }
    }
}
