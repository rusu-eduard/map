package socialnetwork.domain;

import java.io.Serializable;

public class Entity<ID> implements Serializable {

    private static final long serialVersionUID = 7331115341259248461L;

    private ID id;

    /**
     * Method that returns the ID of an entity
     * @return The id of an entity
     */
    public ID getId() {
        return id;
    }

    /**
     * Method that sets the id of an entity
     * @param id - id to be set
     */
    public void setId(ID id) {
        this.id = id;
    }
}