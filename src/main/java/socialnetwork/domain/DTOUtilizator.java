package socialnetwork.domain;
import socialnetwork.utils.Constants;
import java.time.LocalDateTime;

public class DTOUtilizator {

    private final long id;
    private final String firstName;
    private final String lastName;
    private final String date;

    public DTOUtilizator(long id, String firstName, String lastName, LocalDateTime date) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.date = date.format(Constants.DATE_TIME_FORMATTER);;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public long getId() {
        return id;
    }

    public String getDate() {
        return date;
    }

    @Override
    public String toString() {
        return "DTOUtilizator{" +
                "id=" + id +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", date='" + date + '\'' +
                '}';
    }
}
