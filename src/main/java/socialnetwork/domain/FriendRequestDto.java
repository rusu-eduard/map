package socialnetwork.domain;

import java.time.LocalDateTime;

public class FriendRequestDto {
    private FriendRequestStatus status;
    String nameTo;
    String nameFrom;
    Long idTo;
    Long idFrom;
    LocalDateTime date;

    public FriendRequestDto(FriendRequestStatus status, String nameTo, String nameFrom, Long idTo, Long idFrom, LocalDateTime date) {
        this.status = status;
        this.nameTo = nameTo;
        this.nameFrom = nameFrom;
        this.idTo = idTo;
        this.idFrom = idFrom;
        this.date = date;
    }

    public String getStatus() {
        switch (status){
            case PENDING:
                return "Pending";
            case APPROVED:
                return "Approved";
            case DECLINED:
                return "Declined";
            default:
                return null;
        }
    }

    public void setStatus(FriendRequestStatus status) {
        this.status = status;
    }

    public String getNameTo() {
        return nameTo;
    }

    public void setNameTo(String nameTo) {
        this.nameTo = nameTo;
    }

    public String getNameFrom() {
        return nameFrom;
    }

    public void setNameFrom(String nameFrom) {
        this.nameFrom = nameFrom;
    }

    public Long getIdTo() {
        return idTo;
    }

    public void setIdTo(Long idTo) {
        this.idTo = idTo;
    }

    public Long getIdFrom() {
        return idFrom;
    }

    public void setIdFrom(Long idFrom) {
        this.idFrom = idFrom;
    }

    public LocalDateTime getDate() {
        return date;
    }

    public void setDate(LocalDateTime date) {
        this.date = date;
    }
}
