package socialnetwork.domain;

import socialnetwork.exceptions.EventException;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Event extends Entity<Long>{
    private EventStatus status;
    private Long organizer;
    private String name;
    private String description;
    private LocalDateTime startDate;
    private  Map<Long,Boolean> participants;

    public Event(Long organizer, String name, String description, LocalDateTime startDate) {
        this.status = EventStatus.COMING;
        this.organizer = organizer;
        this.name = name;
        this.description = description;
        this.startDate = startDate;
        participants = new HashMap<>();
    }


    public Event(Long organizer, String name, String description, LocalDateTime startDate, Map<Long, Boolean> participants, EventStatus status) {
        this.status = status;
        this.organizer = organizer;
        this.name = name;
        this.description = description;
        this.startDate = startDate;
        this.participants = participants;
    }

    public EventStatus getStatus() {
        return status;
    }

    public String getDescription() {
        return description;
    }

    public LocalDateTime getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDateTime startDate) {
        this.startDate = startDate;
    }

    public Long getOrganizer() {
        return organizer;
    }

    public void setOrganizer(Long organizer) {
        this.organizer = organizer;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Map<Long, Boolean> getParticipants() {
        return participants;
    }

    public void addParticipant(Long id){
        if(!participants.containsKey(id)){
            participants.put(id, true);
        }
        else
            throw new EventException("This user is already subscribed to the event");
    }

    public void removeParticipant(Long id){
        participants.remove(id);
    }

    public void notifyUser(Long id){
        participants.replace(id, true);
    }

    public void unnotifyUser(Long id){
        participants.replace(id, false);
    }
}
