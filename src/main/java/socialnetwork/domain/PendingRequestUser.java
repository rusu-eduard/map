package socialnetwork.domain;


public class PendingRequestUser extends Entity<Long> {
    private final String firstName;
    private final String lastName;
    private final Boolean pendingRequest;

    public PendingRequestUser(Utilizator user, Boolean pendingRequest) {
        this.firstName = user.getFirstName();
        this.lastName = user.getLastName();
        this.pendingRequest = pendingRequest;
        this.setId(user.getId());
    }

    public Utilizator getUser(){
        Utilizator user = new Utilizator(firstName,lastName);
        user.setId(this.getId());
        return user;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public Boolean getPendingRequest() {
        return pendingRequest;
    }
}
