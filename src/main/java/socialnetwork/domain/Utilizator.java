package socialnetwork.domain;
import java.util.Objects;

public class Utilizator extends Entity<Long>{
    private String firstName;
    private String lastName;
    private String password;

    public Utilizator(String firstName, String lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public Utilizator(String firstName, String lastName, String password) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.password = password;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String encryptPassword(){
        char[] chars = this.password.toCharArray();
        char[] encrypted = new char[chars.length];
        for(int i = 0; i < chars.length; i++){
            char c = chars[i];
            c += 5;
            encrypted[i] = c;
        }
        return new String(encrypted);
    }

    public String decryptPassword(){
        char[] chars = this.password.toCharArray();
        char[] decrypted = new char[chars.length];
        for(int i = 0; i < chars.length; i++){
            char c = chars[i];
            c -= 5;
            decrypted[i] = c;
        }
        return new String(decrypted);
    }

    /**
     *
     * @return firstName field of Utilizator
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * Sets the firstName field of Utiliaztor to firstName(param)
     * @param firstName String
     */
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    /**
     *
     * @return lastName field of Utilizator
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * Sets the lastName field of Utilizator to lastName(param)
     * @param lastName String
     */
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }


    @Override
    public String toString() {
        return "Utilizator{" +
                "Id = '" + this.getId() + '\'' +
                "firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Utilizator)) return false;
        Utilizator that = (Utilizator) o;
        return getLastName().equals(that.getLastName()) && getFirstName().equals(that.getFirstName());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getFirstName(), getLastName());
    }

}
