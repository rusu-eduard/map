package socialnetwork.domain;

import java.time.LocalDateTime;

public class FriendRequest extends Entity<Long>{
    private FriendRequestStatus status;
    long from;
    long to;
    LocalDateTime date;

    public FriendRequest(long from, long to)  {
        this.status = FriendRequestStatus.PENDING;
        this.from = from;
        this.to = to;
        this.date = LocalDateTime.now();
    }

    public FriendRequestStatus getStatus() {
        return status;
    }

    public void setStatus(FriendRequestStatus status) {
        this.status = status;
    }

    public long getFrom() {
        return from;
    }

    public long getTo() {
        return to;
    }

    public LocalDateTime getDate() {
        return date;
    }

    public void setDate(LocalDateTime date) {
        this.date = date;
    }

    @Override
    public String toString() {
        return "FriendRequest from: " + from +
                " to: " + to +
                " Status: " + status;
    }
}
