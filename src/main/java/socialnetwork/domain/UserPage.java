package socialnetwork.domain;

import java.util.List;
import java.util.Map;

public class UserPage {
    private final long uId;
    private final String lastName;
    private final String firstName;
    private final long nrOfFrineds;
    private final List<DTOUtilizator> friends;
    private final Map<Utilizator, List<MessageDTO>> conversations;
    private final Map<Group, List<MessageDTO>> conversationsGroups;
    private final List<Utilizator> pendingRequests;
    private final List<FriendRequest> friendRequestsHistory;

    public UserPage(long uId, String lastName, String firstName, long nrOfFrineds, List<DTOUtilizator> friends, Map<Utilizator, List<MessageDTO>> conversations, Map<Group, List<MessageDTO>> conversationsGroups, List<Utilizator> pendingRequests, List<FriendRequest> friendRequestsHistory) {
        this.uId = uId;
        this.lastName = lastName;
        this.firstName = firstName;
        this.nrOfFrineds = nrOfFrineds;
        this.friends = friends;
        this.conversations = conversations;
        this.conversationsGroups = conversationsGroups;
        this.pendingRequests = pendingRequests;
        this.friendRequestsHistory = friendRequestsHistory;
    }

    public List<Utilizator> getPendingRequests() {
        return pendingRequests;
    }

    public List<FriendRequest> getFriendRequestsHistory() {
        return friendRequestsHistory;
    }

    public long getNrOfFrineds() {
        return nrOfFrineds;
    }

    public long getuId() {
        return uId;
    }

    public String getLastName() {
        return lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public List<DTOUtilizator> getFriends() {
        return friends;
    }

    public Map<Utilizator, List<MessageDTO>> getConversations() {
        return conversations;
    }

    public Map<Group, List<MessageDTO>> getConversationsGroups() {
        return conversationsGroups;
    }
}
