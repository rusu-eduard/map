package socialnetwork.domain;

public enum FriendRequestStatus {
    PENDING,APPROVED,DECLINED;
}
