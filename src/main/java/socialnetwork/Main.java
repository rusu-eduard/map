package socialnetwork;

import socialnetwork.config.ApplicationContext;
import socialnetwork.domain.Prietenie;
import socialnetwork.domain.Tuple;
import socialnetwork.repository.Repository;
import socialnetwork.repository.database.DBFriendshipRepository;
import socialnetwork.repository.database.DBMessageRepo;
import socialnetwork.repository.database.DBRequestsRepo;
import socialnetwork.repository.database.DBUserRepository;
import socialnetwork.service.UtilizatorService;

import java.time.LocalDateTime;


public class Main {
    public static void main(String[] args) {

        //Database:
//        final String url = ApplicationContext.getPROPERTIES().getProperty("database.socialnetwork.url");
//        final String username = ApplicationContext.getPROPERTIES().getProperty("database.socialnetwork.username");
//        final String password = ApplicationContext.getPROPERTIES().getProperty("database.socialnetwork.password");
//
//        Repository<Tuple<Long,Long>, Prietenie> friendshipDBRepository = new DBFriendshipRepository(url, username, password);
//
//        Repository<Long, Utilizator> userDBRepository = new DBUserRepository(url,username,password);
//
//        DBMessageRepo messageDBRepo = new DBMessageRepo(url,username,password);
//
//        DBRequestsRepo requestsRepo = new DBRequestsRepo(url,username,password);
//
//        UtilizatorService service = new UtilizatorService(userDBRepository, friendshipDBRepository, messageDBRepo, requestsRepo);
//
//        Ui ui = new Ui(service);
//        ui.run();
        MainFX.main(args);
    }
}
