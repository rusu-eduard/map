package controller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import socialnetwork.domain.Entity;
import socialnetwork.domain.Utilizator;
import socialnetwork.service.ServiceWithUser;
import socialnetwork.service.UtilizatorService;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

public class NewGroupController {

    @FXML
    private TextField groupNameTextField;

    @FXML
    private TableView<Utilizator> allUsersTableView;

    @FXML
    private TableColumn<Utilizator, String> allLastTableColumn;

    @FXML
    private TableColumn<Utilizator, String> allFirstTableColumn;

    @FXML
    private TableView<Utilizator> groupMembersTableView;

    @FXML
    private TableColumn<Utilizator, String> groupLastTableColumn;

    @FXML
    private TableColumn<Utilizator, String> groupFirstTableColumn;

    @FXML
    private Button addToGroupButton;

    @FXML
    private Button removeFromGroupButton;

    private ServiceWithUser service;

    ObservableList<Utilizator> allUsers = FXCollections.observableArrayList();
    ObservableList<Utilizator> groupModel = FXCollections.observableArrayList();

    @FXML
    private void initialize() {
        allLastTableColumn.setCellValueFactory(new PropertyValueFactory<>("lastName"));
        allFirstTableColumn.setCellValueFactory(new PropertyValueFactory<>("firstName"));
        allUsersTableView.setItems(allUsers);

        groupLastTableColumn.setCellValueFactory(new PropertyValueFactory<>("lastName"));
        groupFirstTableColumn.setCellValueFactory(new PropertyValueFactory<>("firstName"));
        groupMembersTableView.setItems(groupModel);
    }

    public void setService(ServiceWithUser service){
        this.service = service;
        initAllUsersModel();
    }

    private void initAllUsersModel() {
        Iterable<Utilizator> users = service.getAllExceptMe();
        List<Utilizator> usersList = StreamSupport.stream(users.spliterator(), false)
                .collect(Collectors.toList());
        allUsers.setAll(usersList);
    }

    @FXML
    void addToGroupButtonHandle(ActionEvent event) {
        Utilizator user = allUsersTableView.getSelectionModel().getSelectedItem();
        if(user == null){
            AlertMessage.showErrorMessage(null, "No user selected!\n");
        }
        else{
            if(groupModel.contains(user)){
                AlertMessage.showErrorMessage(null, "User already added!\n");
            }
            else{
                groupModel.add(user);
                allUsers.remove(user);
            }
        }
    }

    @FXML
    void cancelButtonHandle(ActionEvent event) {
        Stage window = (Stage) ((Node) event.getSource()).getScene().getWindow();
        window.close();
    }

    @FXML
    void createGroupButtonHandle(ActionEvent event) {
        String groupName = groupNameTextField.getText();
        List<Long> members = groupModel.stream()
                .map(Entity::getId)
                .collect(Collectors.toList());
        StringBuilder err = new StringBuilder();
        if(groupName.isEmpty())
            err.append("Group name cannot be empty\n");
        if(members.isEmpty())
            err.append("There has to be at least one member in order to create a group!\n");
        if(!err.toString().isEmpty()){
            AlertMessage.showErrorMessage(null, err.toString());
        }
        else{
            service.createGroup(groupName, members);
            AlertMessage.showPopUp(null, "Group was created");
            Stage window = (Stage) ((Node) event.getSource()).getScene().getWindow();
            window.close();
        }
    }

    @FXML
    void removeFromGroupButtonHandle(ActionEvent event) {
        Utilizator user = groupMembersTableView.getSelectionModel().getSelectedItem();
        if(user == null){
            AlertMessage.showErrorMessage(null, "No member selected!");
        }
        else {
            groupModel.remove(user);
            allUsers.add(user);
        }
    }

//    public void allUsersClicked(MouseEvent mouseEvent) {
//        groupMembersTableView.getSelectionModel().clearSelection();
//    }

    public void groupUsersClicked(MouseEvent mouseEvent) {
        allUsersTableView.getSelectionModel().clearSelection();
    }
}