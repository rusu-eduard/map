package controller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import socialnetwork.domain.DTOUtilizator;
import socialnetwork.domain.PendingRequestUser;
import socialnetwork.domain.Utilizator;
import socialnetwork.exceptions.ServiceException;
import socialnetwork.service.ServiceWithUser;
import socialnetwork.utils.events.PrietenieChangeEvent;
import socialnetwork.utils.observer.Observer;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

public class ProfileController implements Observer<PrietenieChangeEvent> {


    @FXML
    private ImageView profileImageView;

    @FXML
    private ImageView allFriendImageView;

    @FXML
    private AnchorPane friendsPane;

    @FXML
    private TableView<DTOUtilizator> friendsTableView;

    @FXML
    private TableColumn<DTOUtilizator, String> lastNameTableColumn;

    @FXML
    private TableColumn<DTOUtilizator, String> firstNameTableColumn;

    @FXML
    private TextField searchTextField;

    @FXML
    private AnchorPane userPane;

    @FXML
    private Button sendRequestButton;

    @FXML
    private Button cancelButton;

    @FXML
    private Button unfriendButton;

    @FXML
    private Label firstNameLabel;

    @FXML
    private Label lastNameLabel;

    @FXML
    private Label nrOfFriendsLabel;

    @FXML
    private Label idLabel;

    private ServiceWithUser service;
    private Utilizator mainUser;

    ObservableList<DTOUtilizator> friendsModel = FXCollections.observableArrayList();

    @FXML
    private void initialize() {
            lastNameTableColumn.setCellValueFactory(new PropertyValueFactory<>("lastName"));
            firstNameTableColumn.setCellValueFactory(new PropertyValueFactory<>("firstName"));
            friendsTableView.setItems(friendsModel);
    }

    public void setServiceAndUser(ServiceWithUser service, Utilizator user){
        this.mainUser = user;
        this.service = service;
        service.getService().addObserver(this);
        setLabels();
        setButtons();
        initModel();
    }

    private void initModel() {
        List<DTOUtilizator> friendList = StreamSupport.stream(service.getFriends().spliterator(), false)
                .collect(Collectors.toList());
        friendsModel.setAll(friendList);
    }

    private void setButtons() {
        if(service.getService().findOneFriendship(service.getUser().getId(), mainUser.getId()) != null){
            unfriendButton.setVisible(true);
            sendRequestButton.setVisible(false);
            cancelButton.setVisible(false);
        }
        else if(service.getService().FindRequestBetween(service.getUser().getId(), mainUser.getId())){
            unfriendButton.setVisible(false);
            sendRequestButton.setVisible(false);
            cancelButton.setVisible(true);
        }
        else{
            unfriendButton.setVisible(false);
            sendRequestButton.setVisible(true);
            cancelButton.setVisible(false);
        }
    }

    private void setLabels() {
        firstNameLabel.setText(service.getUser().getFirstName());
        lastNameLabel.setText(service.getUser().getLastName());
        idLabel.setText(service.getUser().getId().toString());
        nrOfFriendsLabel.setText(service.getNrOfFriends().toString());
    }

    @FXML
    void allFriendsButtonHandle(ActionEvent event) {
        profileImageView.setImage(new Image(String.valueOf(getClass().getResource("/images/user.png"))));
        allFriendImageView.setImage(new Image(String.valueOf(getClass().getResource("/images/friends_selected.png"))));
        userPane.setVisible(false);
        friendsPane.setVisible(true);
    }

    @FXML
    void cancelButtonHandle(ActionEvent event) {
        service.getService().deleteRequest(mainUser.getId(), service.getUser().getId());
    }

    @FXML
    void checkProfileButtonHandle(ActionEvent event) {
        try {
            DTOUtilizator user = friendsTableView.getSelectionModel().getSelectedItem();
            if(user == null){
                AlertMessage.showErrorMessage(null, "No user was selected form the table!");
            }
            else {
                Utilizator profileUser = service.findOne(user.getId());


                FXMLLoader loader = new FXMLLoader();
                loader.setLocation(getClass().getResource("/views/profileView.fxml"));
                Parent root = loader.load();

                ProfileController controller = loader.getController();
                controller.setServiceAndUser(new ServiceWithUser(profileUser, service.getService()), mainUser);

                Scene profileScene = new Scene(root);

                Stage window = (Stage) ((Node) event.getSource()).getScene().getWindow();
                window.setScene(profileScene);

                window.show();
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @FXML
    void myProfileButtonHandle(ActionEvent event) {
        try{
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("/views/userView.fxml"));
            Parent userViewParent = loader.load();

            UserController userController = loader.getController();
            userController.setService(new ServiceWithUser(mainUser, service.getService()));

            Scene singInScene = new Scene(userViewParent);

            Stage window = (Stage) ((Node) event.getSource()).getScene().getWindow();
            window.setTitle("Social network");
            window.setScene(singInScene);

            window.show();
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }

    @FXML
    void profileButtonHandle(ActionEvent event) {
        profileImageView.setImage(new Image(String.valueOf(getClass().getResource("/images/user_selected.png"))));
        allFriendImageView.setImage(new Image(String.valueOf(getClass().getResource("/images/friends.png"))));
        userPane.setVisible(true);
        friendsPane.setVisible(false);
    }

    @FXML
    void sendRequestButtonHandle(ActionEvent event) {
        try {
            service.getService().sendRequest(mainUser.getId(), service.getUser().getId());
        }
        catch (ServiceException err){
            AlertMessage.showErrorMessage(null, err.getMessage());
        }
    }

    @FXML
    void unfriendButtonHandle(ActionEvent event) {
        service.getService().removeFriendship(mainUser.getId(), service.getUser().getId());
    }

    @Override
    public void update(PrietenieChangeEvent prietenieChangeEvent) {
        setLabels();
        setButtons();
        initModel();
    }

    public void sendMessageButtonHandle(ActionEvent actionEvent) {
        try {
            // create a new stage for the popup dialog.
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("/views/composeMessageView.fxml"));

            Parent root = loader.load();

            ComposeMessageController controller = loader.getController();
            controller.setService(service.getService(), mainUser, service.getUser());

            // Create the dialog Stage.
            Stage dialogStage = new Stage();
            dialogStage.initModality(Modality.NONE);
            //dialogStage.initOwner(primaryStage);
            Scene scene = new Scene(root);
            dialogStage.setScene(scene);

            dialogStage.show();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
