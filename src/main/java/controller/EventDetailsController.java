package controller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import socialnetwork.domain.Event;
import socialnetwork.domain.Utilizator;
import socialnetwork.service.ServiceWithUser;
import java.util.List;
import java.util.stream.Collectors;

public class EventDetailsController {

    private ServiceWithUser service;
    private Event ev;

    @FXML
    private Button joinEventButton;

    @FXML
    private Button leaveEventButton;

    @FXML
    private Button subscribeNotificationsButton;

    @FXML
    private Button unsubscribeNotificationsButton;

    @FXML
    private Label organizerLabel;

    @FXML
    private Label eventNameLabel;

    @FXML
    private TextArea eventDescriptionTextArea;

    @FXML
    private TableView<Utilizator> participantsTableView;

    @FXML
    private TableColumn<Utilizator, String> lastNameTableColumn;

    @FXML
    private TableColumn<Utilizator, String> firstNameTableColumn;

    ObservableList<Utilizator> evParticipants = FXCollections.observableArrayList();

    @FXML
    private void initialize() {
        lastNameTableColumn.setCellValueFactory(new PropertyValueFactory<>("lastName"));
        firstNameTableColumn.setCellValueFactory(new PropertyValueFactory<>("firstName"));
        participantsTableView.setItems(evParticipants);
    }

    private void initParticipantsModel(){
        List<Utilizator> usersList = ev.getParticipants().keySet().stream()
                .map(x -> service.getService().findOne(x))
                .collect(Collectors.toList());
        evParticipants.setAll(usersList);
    }

    public void setService(ServiceWithUser service, Event ev){
        this.service = service;
        this.ev = ev;
        initParticipantsModel();
        organizerLabel.setText(service.getService().findOne(ev.getOrganizer()).getLastName());
        eventNameLabel.setText(ev.getName());
        eventDescriptionTextArea.setText(ev.getDescription());
        if(ev.getParticipants().containsKey(service.getUser().getId())){
            joinEventButton.setVisible(false);
            leaveEventButton.setVisible(true);
            if(ev.getParticipants().get(service.getUser().getId())){
                unsubscribeNotificationsButton.setVisible(true);
                subscribeNotificationsButton.setVisible(false);
            }
            else{
                unsubscribeNotificationsButton.setVisible(false);
                subscribeNotificationsButton.setVisible(true);
            }
        }
        else{
            leaveEventButton.setVisible(false);
            joinEventButton.setVisible(true);
            unsubscribeNotificationsButton.setVisible(false);
            subscribeNotificationsButton.setVisible(false);
        }
    }

    @FXML
    void JoinEventButtonHandle(ActionEvent event) {
        ev.addParticipant(service.getUser().getId());
        service.getService().addParticipantToEv(ev, service.getUser().getId());
        joinEventButton.setVisible(false);
        leaveEventButton.setVisible(true);
        unsubscribeNotificationsButton.setVisible(true);
        subscribeNotificationsButton.setVisible(false);
    }

    @FXML
    void SubscribeButtonHandle(ActionEvent event) {
        subscribeNotificationsButton.setVisible(false);
        unsubscribeNotificationsButton.setVisible(true);
        ev.notifyUser(service.getUser().getId());
        service.getService().notifyUser(ev, service.getUser().getId());
    }

    @FXML
    void UnsubscribeButtonHandle(ActionEvent event) {
        subscribeNotificationsButton.setVisible(true);
        unsubscribeNotificationsButton.setVisible(false);
        ev.unnotifyUser(service.getUser().getId());
        service.getService().unnotifyUser(ev, service.getUser().getId());
    }

    public void leaveEventButtonHandle(ActionEvent actionEvent) {
        ev.removeParticipant(service.getUser().getId());
        service.getService().removeParticipantFromEv(ev, service.getUser().getId());
        leaveEventButton.setVisible(false);
        subscribeNotificationsButton.setVisible(false);
        unsubscribeNotificationsButton.setVisible(false);
        joinEventButton.setVisible(true);
    }
}