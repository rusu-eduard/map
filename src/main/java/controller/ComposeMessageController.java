package controller;

import javafx.event.ActionEvent;

import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import socialnetwork.domain.Message;
import socialnetwork.domain.Utilizator;
import socialnetwork.exceptions.ServiceException;
import socialnetwork.service.ServiceWithUser;
import socialnetwork.service.UtilizatorService;

public class ComposeMessageController {

    @FXML
    private TextField toTextField;

    @FXML
    private TextArea messageTextArea;

    private Utilizator from;

    private Utilizator to;

    private UtilizatorService service;

    public void setService(UtilizatorService service, Utilizator from, Utilizator to){
        this.service = service;
        this.from = from;
        this.to = to;
        toTextField.setText(to.getLastName());
    }

    @FXML
    void cancelButtonHandle(ActionEvent event) {
        Stage window = (Stage) ((Node) event.getSource()).getScene().getWindow();
        window.close();
    }

    @FXML
    void sendButtonHandle(ActionEvent event) {
        String text = messageTextArea.getText();
        if(text.isEmpty()){
            AlertMessage.showErrorMessage(null, "No message written!\n");
        }
        else {
            service.sendMessage(from.getId(), text, to.getId(), null);
            AlertMessage.showPopUp(null, "Message sent successfully!\n");
            Stage window = (Stage) ((Node) event.getSource()).getScene().getWindow();
            window.close();
        }
    }
}