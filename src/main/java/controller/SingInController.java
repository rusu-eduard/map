package controller;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import socialnetwork.domain.Utilizator;
import socialnetwork.service.UtilizatorService;

import java.io.IOException;

public class SingInController {
    private UtilizatorService service;

    public void setService(UtilizatorService service){
        this.service = service;
    }

    @FXML
    private void initialize() {
    }

    @FXML
    private TextField firstNameField;

    @FXML
    private TextField lastNameField;

    @FXML
    private PasswordField passwordField;

    @FXML
    private PasswordField confirmPasswordField;


    @FXML
    void handleCreateAccountButton(ActionEvent event) {
        String firstName = firstNameField.getText();
        if(firstName.isEmpty()){
            AlertMessage.showErrorMessage(null, "First name was not inserted");
        }

        String lastName = lastNameField.getText();
        if(lastName.isEmpty()){
            AlertMessage.showErrorMessage(null, "Last name was not inserted");
        }

        String password = passwordField.getText();
        if(password.isEmpty()){
            AlertMessage.showErrorMessage(null, "Please insert a password!");
        }

        String confirmedPassword = confirmPasswordField.getText();
        if(confirmedPassword.isEmpty()){
            AlertMessage.showErrorMessage(null, "Please confirm your password");
        }

        if(!password.equals(confirmedPassword)){
            AlertMessage.showErrorMessage(null, "Password confirmation error! Please enter the same password!");
        }

        String name = lastName + " " + firstName;
        try {
            Utilizator user = service.save(firstName, lastName, password);
            String userId = Long.toString(user.getId());
            String userName = user.getLastName() + " " + user.getFirstName();
            AlertMessage.showPopUp(null,"Your account was created!\nYour id is : " + userId);

            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("/views/logInView.fxml"));
            Parent root = loader.load();

            LogInController logInController = loader.getController();
            logInController.setLogInControllerService(service);

            Scene logInScene = new Scene(root);

            Stage window = (Stage) ((Node) event.getSource()).getScene().getWindow();
            window.setScene(logInScene);

            window.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void HandleBackButton(ActionEvent actionEvent) {
        try {
            Parent singInParent = FXMLLoader.load(getClass().getResource("/views/logInView.fxml"));
            Scene singInScene = new Scene(singInParent);

            Stage window = (Stage) ((Node)actionEvent.getSource()).getScene().getWindow();
            window.setScene(singInScene);
            window.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
