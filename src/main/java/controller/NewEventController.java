package controller;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import socialnetwork.domain.Event;
import socialnetwork.service.ServiceWithUser;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

public class NewEventController {

    @FXML
    private TextField nameTextField;

    @FXML
    private TextArea descriptionTextArea;

    @FXML
    private DatePicker datePicker;

    @FXML
    private ChoiceBox<LocalTime> timeChoiceBox;

    private ServiceWithUser service;

    @FXML
    private void initialize() {
    }

    public void setService(ServiceWithUser service){
        this.service = service;
        timeChoiceBox.getItems().addAll(
                LocalTime.of(10,0),
                LocalTime.of(10,30),
                LocalTime.of(11,0),
                LocalTime.of(11,30),
                LocalTime.of(12,0),
                LocalTime.of(12,30),
                LocalTime.of(13,0),
                LocalTime.of(13,30),
                LocalTime.of(14,0),
                LocalTime.of(14,30),
                LocalTime.of(15,0),
                LocalTime.of(15,30),
                LocalTime.of(16,0),
                LocalTime.of(16,30),
                LocalTime.of(17,0),
                LocalTime.of(17,30),
                LocalTime.of(18,0),
                LocalTime.of(18,30),
                LocalTime.of(19,0),
                LocalTime.of(19,30),
                LocalTime.of(20,0),
                LocalTime.of(20,30),
                LocalTime.of(21,0),
                LocalTime.of(21,30),
                LocalTime.of(22,0),
                LocalTime.of(22,30),
                LocalTime.of(23,0),
                LocalTime.of(23,30)
        );
    }

    @FXML
    void CancelButtonHandle(ActionEvent event) {
        Stage window = (Stage) ((Node) event.getSource()).getScene().getWindow();
        window.close();
    }

    @FXML
    void CreateEventButtonHandle(ActionEvent event) {
        String name = nameTextField.getText();
        String desc = descriptionTextArea.getText();
        LocalDate date = datePicker.getValue();
        LocalTime time = timeChoiceBox.getValue();
        String err = "";

        if(name.equals(""))
            err += "Name area must be filled!\n";
        if(desc.equals(""))
            err += "A description must be added!\n";
        if(date == null){
            err += "No date was selected!\n";
        }
        else if(date.isBefore(LocalDateTime.now().toLocalDate()))
            err += "Invalid date picked!\n";
        if(time == null) {
            err += "You must chose an hour at witch the event starts!\n";
        }
        if(date != null && time != null && date.isEqual(LocalDateTime.now().toLocalDate()) && time.isBefore(LocalDateTime.now().toLocalTime())){
            err += "Invalid time picked!\n";
        }
        if(!err.equals("")){
            AlertMessage.showErrorMessage(null, err);
        }
        else{
            LocalDateTime dateTime = time.atDate(date);
            Event ev = new Event(service.getUser().getId(), name, desc, dateTime);
            service.getService().saveEvent(ev);
            AlertMessage.showPopUp(null, "The event was created successfully!");
            Stage window = (Stage) ((Node) event.getSource()).getScene().getWindow();
            window.close();
        }
    }
}