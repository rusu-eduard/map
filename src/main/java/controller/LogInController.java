package controller;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.paint.Color;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import socialnetwork.domain.Utilizator;
import socialnetwork.service.ServiceWithUser;
import socialnetwork.service.UtilizatorService;

import java.io.IOException;

public class LogInController {
    private UtilizatorService service;

    @FXML
    private TextField idTextField;

    @FXML
    private PasswordField passwordTextField;

    public void setLogInControllerService(UtilizatorService service){
        this.service = service;
    }

    @FXML
    private void initialize() {
    }

    public void SignInButtonAction(ActionEvent event) {
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("/views/signInView.fxml"));
            Parent singInParent = loader.load();
            Scene singInScene = new Scene(singInParent);

            SingInController controller = loader.getController();
            controller.setService(service);

            Stage window = (Stage) ((Node) event.getSource()).getScene().getWindow();
            window.setTitle("Social network");
            window.setScene(singInScene);
            window.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void LogInButtonHandle(ActionEvent event) {
        String id = idTextField.getText();
        String password = passwordTextField.getText();
        if(id.isEmpty())
            AlertMessage.showPopUp(null,"No id was inserted!");
        else {
            try {
                Utilizator user = service.findOne(Long.parseLong(id));
                if(user == null){
                    AlertMessage.showErrorMessage(null, "There is no user with given id");
                }
                else
                    if(!password.equals(user.getPassword())) {
                        AlertMessage.showErrorMessage(null, "Password is incorrect!");
                }
                else {
                    FXMLLoader loader = new FXMLLoader();
                    loader.setLocation(getClass().getResource("/views/userView.fxml"));
                    Parent userViewParent = loader.load();

                    UserController userController = loader.getController();
                    userController.setService(new ServiceWithUser(user, service));

                    Scene singInScene = new Scene(userViewParent);

                    Stage window = (Stage) ((Node) event.getSource()).getScene().getWindow();
                    window.setTitle("Social network");
                    window.setScene(singInScene);

                    window.show();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
