package controller;

import com.itextpdf.text.*;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.ScrollEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
import socialnetwork.domain.*;
import socialnetwork.service.ServiceWithUser;
import socialnetwork.service.UtilizatorService;
import socialnetwork.utils.Constants;
import socialnetwork.utils.events.PrietenieChangeEvent;
import socialnetwork.utils.observer.Observer;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

public class UserController implements Observer<PrietenieChangeEvent> {

    @FXML
    private ImageView profileImageView;

    @FXML
    private ImageView eventImageView;

    @FXML
    private ImageView notificationImageView;

    @FXML
    private ImageView addFriendImageView;

    @FXML
    private ImageView chatImageView;

    @FXML
    private ImageView allFriendImageView;

    @FXML
    private ImageView logOutImageView;

    @FXML
    private ImageView requestsImageView;

    @FXML
    private ImageView pdfImageView;

    @FXML
    private AnchorPane userPane;

    @FXML
    private Label firstNameLabel;

    @FXML
    private Label lastNameLabel;

    @FXML
    private Label nrOfFriendsLabel;

    @FXML
    private Label idLabel;

    @FXML
    private Label friendRequestNotificationLabel;

    @FXML
    private AnchorPane messagePane;

    @FXML
    private AnchorPane friendsPane;

    @FXML
    private AnchorPane searchUserPane;

    @FXML
    private TextField searchAllUsers;

    @FXML
    private TextArea messageTextArea;

    @FXML
    private AnchorPane eventsPane;

    @FXML
    private AnchorPane requestsPane;

    @FXML
    private TableView<Utilizator> pendingRequestsTableView;

    @FXML
    private TableColumn<Utilizator, String> lastNamePendingTableColumn;

    @FXML
    private TableColumn<Utilizator, String> firstNamePendingTableColumn;

    @FXML
    private TextField searchRequestUser;

    @FXML
    private TableView<FriendRequestDto> requestsHistoryTableView;

    @FXML
    private TableView<Utilizator> userConversationTableView;

    @FXML
    private TableColumn<Utilizator, String> userConversationLastNameColumn;

    @FXML
    private TableColumn<Utilizator, String> userConversationFirstNameColumn;

    @FXML
    private TableColumn<FriendRequestDto, String> fromHistoryTableColumn;

    @FXML
    private TableColumn<FriendRequestDto, String> toHistoryTableColumn;

    @FXML
    private TableColumn<FriendRequestDto, String> statusHistoryTableColumn;

    @FXML
    private TableColumn<FriendRequestDto, LocalDateTime> dateHistoryTableColumn;

    @FXML
    private TabPane pdfPane;

    @FXML
    private DatePicker pdf1StartDatePicker;

    @FXML
    private DatePicker pdf1EndDatePicker;

    @FXML
    private TableView<DTOUtilizator> pdf1FriendsTableView;

    @FXML
    private TableColumn<DTOUtilizator, String> pdf1FriendsNameColumn;

    @FXML
    private TableColumn<DTOUtilizator, LocalDateTime> pdf1FriendsDateColumn;

    @FXML
    private TableView<MessageDTO> pdf1MessagesTableView;

    @FXML
    private TableColumn<MessageDTO, String> pdf1MessagesFromColumn;

    @FXML
    private TableView<MessageDTO> messageTableView;

    @FXML
    private TableColumn<MessageDTO, String> messageTableColumn;

    @FXML
    private TableColumn<MessageDTO, LocalDateTime> pdf1MessagesDateColumn;

    @FXML
    private TableColumn<MessageDTO, String> pdf1MessagesMsgColumn;

    @FXML
    private ChoiceBox<String> pdf2ChoiceBox;

    @FXML
    private DatePicker pdf2StartDate;

    @FXML
    private DatePicker pdf2EndDate;

    @FXML
    private TableView<MessageDTO> pdf2Table;

    @FXML
    private TableColumn<MessageDTO, LocalDateTime> pdf2DateTableColumn;

    @FXML
    private TableColumn<MessageDTO, String> pdf2MessageTableColumn;

    @FXML
    private Button myProfileButton;


    private ServiceWithUser service;

    private final ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(2);

    public void setService(ServiceWithUser service){
        this.service = service;
        UserPage page = service.getUserPage();
        initPage(page);
        initButtons();
        service.addObserver(this);
//        setLabels();
        initModel();
//        initPendingModel();
//        initHistoryModel();
//        initFriendsModel();
        initConversationsModel();
        setRequestNotificationLabel();
        initChoice();
        initAllEventsModel();
        initMyEventsModel();
//        initGroupsModel();
        initMaxPages();
        periodicEventCheck();
        periodicEventUpdate();
    }

    @FXML
    private Button searchUsersButton;

    @FXML
    private Button messageButton;

    @FXML
    private Button friendsButton;

    @FXML
    private Button logOutButton;

    @FXML
    private Button requestsButton;

    @FXML
    private Button eventsButton;

    @FXML
    private Button pdfButton;

    private void initButtons() {
        myProfileButton.setTooltip(new Tooltip("My profile"));
        searchUsersButton.setTooltip(new Tooltip("Other users"));
        messageButton.setTooltip(new Tooltip("Messages"));
        friendsButton.setTooltip(new Tooltip("My friends"));
        logOutButton.setTooltip(new Tooltip("Log out"));
        requestsButton.setTooltip(new Tooltip("Friend requests"));
        eventsButton.setTooltip(new Tooltip("Events"));
        pdfButton.setTooltip(new Tooltip("PDF"));
    }

    private void initPage(UserPage page) {
        setLabels(page.getFirstName(), page.getLastName(), Long.toString(page.getuId()), Long.toString(page.getNrOfFrineds()));
        initFriendsModel(page.getFriends());
//        initConversationsModel(page.getConversations().keySet());
        initGroupsModel(page.getConversationsGroups().keySet());
        initPendingModel(page.getPendingRequests());
        initHistoryModel(page.getFriendRequestsHistory());
    }

    public void periodicEventCheck(){
        scheduler.scheduleAtFixedRate(new Runnable() {
            @Override
            public void run() {
                List<String> message = myEventsModel.stream()
                        .filter(event -> {
                            LocalDateTime startDate = event.getStartDate();
                            LocalDateTime now = LocalDateTime.now();
                            return startDate.toLocalDate().equals(now.toLocalDate()) && event.getParticipants().get(service.getUser().getId());
                        })
                        .map(ev -> {
                            LocalTime time = ev.getStartDate().toLocalTime();
                            String name = ev.getName();
                            return "The event " + name + " is starting today at " + time + "\n";
                        })
                        .collect(Collectors.toList());
                if(!message.isEmpty()){
                    StringBuilder msg = new StringBuilder();
                    for (String m : message){
                        msg.append(m);
                    }
                    Platform.runLater(() -> {
                        AlertMessage.showPopUp(null, msg.toString());
                    });
                }
            }
        }, 5, 10, TimeUnit.SECONDS);
    }

    public void periodicEventUpdate(){
        scheduler.scheduleAtFixedRate(new Runnable() {
            @Override
            public void run() {
                List<Event> evId = new ArrayList<>();
                allEventsModel
                        .forEach(x->{
                            if(x.getStartDate().compareTo(LocalDateTime.now()) < 0){
                                evId.add(x);
                            }
                        });
                if(!evId.isEmpty()){
                    Platform.runLater(()->{
                        evId.forEach(x->{
                            service.getService().updateEvent(x.getId(), EventStatus.FINISHED);
                            if(x.getParticipants().containsKey(service.getUser().getId()))
                                AlertMessage.showPopUp(null, "Event " + x.getName() + " finished!");
                            initAllEventsModel();
                            initMyEventsModel();
                        });
                    });
                }
            }
        },0, 1, TimeUnit.MINUTES );
    }

    public void showNotifications(){
        if(service.getNotifications() > 0){
            friendRequestNotificationLabel.setText(Integer.toString(service.getNotifications()));
            friendRequestNotificationLabel.setVisible(true);
        }
    }

    private void initChoice() {
        friendsModel.forEach(x->{
            pdf2ChoiceBox.getItems().add(x.getLastName());
        });
    }


    ObservableList<PendingRequestUser> allUsersModel = FXCollections.observableArrayList();
    ObservableList<Utilizator> pendingRequestsModel = FXCollections.observableArrayList();
    ObservableList<Utilizator> userConversationModel = FXCollections.observableArrayList();
    ObservableList<FriendRequestDto> pendingRequestsHistoryModel = FXCollections.observableArrayList();
    ObservableList<DTOUtilizator> friendsModel = FXCollections.observableArrayList();
    ObservableList<DTOUtilizator> pdf1friendsModel = FXCollections.observableArrayList();
    ObservableList<MessageDTO> pdf1receivedMessagesModel = FXCollections.observableArrayList();
    ObservableList<MessageDTO> pdf2receivedMessagesModel = FXCollections.observableArrayList();
    ObservableList<MessageDTO> messagesModel = FXCollections.observableArrayList();
    ObservableList<Event> allEventsModel = FXCollections.observableArrayList();
    ObservableList<Event> myEventsModel = FXCollections.observableArrayList();
    ObservableList<Group> myGroupsModel = FXCollections.observableArrayList();


    @FXML
    private TableView<PendingRequestUser> allUsersTableView;

    @FXML
    private TableView<Group> groupTableView;

    @FXML
    private TableColumn<Group, String> groupNameTableColumn;

    @FXML
    private TableColumn<PendingRequestUser, String> allUsersLastNameTableColumn;

    @FXML
    private TableColumn<PendingRequestUser, String> allUsersFirstNameTableColumn;

    @FXML
    private VBox sendMessageBox;

    @FXML
    private TableView<DTOUtilizator> friendsTableView;

    @FXML
    private TableColumn<DTOUtilizator, String> lastNameFriendsTableColumn;

    @FXML
    private TableColumn<DTOUtilizator, String> firstNameFriendsTableColumn;


    @FXML
    private TableView<MessageDTO> sentMessagesTableView;

    @FXML
    private TableColumn<MessageDTO, LocalDateTime> sentDateTableColumn;

    @FXML
    private TableColumn<MessageDTO, String> sentMessageTableColumn;

    @FXML
    private TableView<MessageDTO> receivedMessagesTableView;

    @FXML
    private TableView<Event> allEventsTableView;

    @FXML
    private TableColumn<Event, String> allEventsNameColumn;

    @FXML
    private TableColumn<Event, LocalDateTime> allEventsStartColumn;

    @FXML
    private TableView<Event> myEventsTableView;

    @FXML
    private TableColumn<Event, String> myEventsNameColumn;

    @FXML
    private TableColumn<Event, LocalDateTime> myEventsStartColumn;

    @FXML
    private TableColumn<MessageDTO, String> receivedMessagesFromTableColumn;

    @FXML
    private TableColumn<MessageDTO, LocalDateTime> receivedMessagesDateTableColumn;

    @FXML
    private TableColumn<MessageDTO, String> receivedMessagesMessageTableColumn;

    @FXML
    private Button pdf1GenerateButton;

    @FXML
    private Button replyOneButton;


    @FXML
    private void initialize() {
        groupNameTableColumn.setCellValueFactory(new PropertyValueFactory<>("name"));
        groupTableView.setItems(myGroupsModel);

        messageTableColumn.setCellValueFactory(new PropertyValueFactory<>("outputMessage"));
        messageTableView.setItems(messagesModel);

        userConversationFirstNameColumn.setCellValueFactory(new PropertyValueFactory<>("firstName"));
        userConversationLastNameColumn.setCellValueFactory(new PropertyValueFactory<>("lastName"));
        userConversationTableView.setItems(userConversationModel);

        myEventsNameColumn.setCellValueFactory(new PropertyValueFactory<>("name"));
        myEventsStartColumn.setCellValueFactory(new PropertyValueFactory<>("startDate"));
        myEventsTableView.setItems(myEventsModel);

        allEventsNameColumn.setCellValueFactory(new PropertyValueFactory<>("name"));
        allEventsStartColumn.setCellValueFactory(new PropertyValueFactory<>("startDate"));
        allEventsTableView.setItems(allEventsModel);

        pdf1MessagesFromColumn.setCellValueFactory(new PropertyValueFactory<>("from"));
        pdf1MessagesDateColumn.setCellValueFactory(new PropertyValueFactory<>("date"));
        pdf1MessagesMsgColumn.setCellValueFactory(new PropertyValueFactory<>("msg"));
        pdf1MessagesTableView.setItems(pdf1receivedMessagesModel);

        pdf2MessageTableColumn.setCellValueFactory(new PropertyValueFactory<>("msg"));
        pdf2DateTableColumn.setCellValueFactory(new PropertyValueFactory<>("date"));
        pdf2Table.setItems(pdf2receivedMessagesModel);

        allUsersFirstNameTableColumn.setCellValueFactory(new PropertyValueFactory<>("firstName"));
        allUsersLastNameTableColumn.setCellValueFactory(new PropertyValueFactory<>("lastName"));
        allUsersTableView.setItems(allUsersModel);

        lastNamePendingTableColumn.setCellValueFactory(new PropertyValueFactory<>("lastName"));
        firstNamePendingTableColumn.setCellValueFactory(new PropertyValueFactory<>("firstName"));
        pendingRequestsTableView.setItems(pendingRequestsModel);

        dateHistoryTableColumn.setCellValueFactory(new PropertyValueFactory<>("date"));
        fromHistoryTableColumn.setCellValueFactory(new PropertyValueFactory<>("nameFrom"));
        toHistoryTableColumn.setCellValueFactory(new PropertyValueFactory<>("nameTo"));
        statusHistoryTableColumn.setCellValueFactory(new PropertyValueFactory<>("status"));
        requestsHistoryTableView.setItems(pendingRequestsHistoryModel);

        firstNameFriendsTableColumn.setCellValueFactory(new PropertyValueFactory<>("firstName"));
        lastNameFriendsTableColumn.setCellValueFactory(new PropertyValueFactory<>("lastName"));
        friendsTableView.setItems(friendsModel);

        pdf1FriendsNameColumn.setCellValueFactory(new PropertyValueFactory<>("lastName"));
        pdf1FriendsDateColumn.setCellValueFactory(new PropertyValueFactory<>("date"));
        pdf1FriendsTableView.setItems(pdf1friendsModel);
    }

    private void initGroupsModel(){
        Iterable<Group> groups = service.getMyGroups();
        List<Group> groupList = StreamSupport.stream(groups.spliterator(), false)
                .collect(Collectors.toList());
        myGroupsModel.setAll(groupList);
    }

    private void initGroupsModel(Set<Group> groups){
        myGroupsModel.setAll(groups);
    }

    private void initMessageModel(Utilizator other, int pageNumber){
        Iterable<MessageDTO> msgs = service.getMessagesWithUser(other.getId(), pageNumber, pageSize);
        List<MessageDTO> msgsList = StreamSupport.stream(msgs.spliterator(), false)
                .collect(Collectors.toList());
        messagesModel.setAll(msgsList);
    }

    private void initMessageModel(Group gr, int pageNumber){
        Iterable<MessageDTO> msgs = service.getMessagesWithGroup(gr.getId(), pageNumber, pageSize);
        List<MessageDTO> msgsList = StreamSupport.stream(msgs.spliterator(), false)
                .collect(Collectors.toList());
        messagesModel.setAll(msgsList);
    }

    private void initConversationsModel(){
        Iterable<Utilizator> users = service.getUsersWithConv();
        List<Utilizator> usersList = StreamSupport.stream(users.spliterator(), false)
                .collect(Collectors.toList());
        userConversationModel.setAll(usersList);
    }

    private void initConversationsModel(Set<Utilizator> users){
        userConversationModel.setAll(users);
    }

    private void initAllEventsModel(){
        Iterable<Event> events = service.getService().getAllEvents();
        List<Event> eventList = StreamSupport.stream(events.spliterator(), false)
                .collect(Collectors.toList());
        allEventsModel.setAll(eventList);
    }

    private void initMyEventsModel(){
        Iterable<Event> events = service.getService().getEventsForUser(service.getUser().getId());
        List<Event> eventList = StreamSupport.stream(events.spliterator(), false)
                .collect(Collectors.toList());
        myEventsModel.setAll(eventList);
    }


    private void initModel() {
        String name = searchAllUsers.getText();
        int pageNumber = Integer.parseInt(usersPageCounter.getText());
        Iterable<PendingRequestUser> messages = service.getPossibleFriends(pageNumber, pageSize, name);
        List<PendingRequestUser> messageTaskList = StreamSupport.stream(messages.spliterator(), false)
                .collect(Collectors.toList());
        allUsersModel.setAll(messageTaskList);
    }

    private void initPendingModel(){
        Iterable<Utilizator> users = service.getPendingRequests();
        List<Utilizator> usersList = StreamSupport.stream(users.spliterator(),false)
                .collect(Collectors.toList());
        pendingRequestsModel.setAll(usersList);
    }

    private void initPendingModel(List<Utilizator> pending){
        pendingRequestsModel.setAll(pending);
    }

    private void initHistoryModel(){
        int pageNumber = Integer.parseInt(requestsPageCounter.getText());
        Iterable<FriendRequest> requests = service.getFriendRequests(pageNumber, pageSize);
        List<FriendRequestDto> requestHistory = StreamSupport.stream(requests.spliterator(),false)
                .map(x->new FriendRequestDto(x.getStatus(),service.findOne(x.getTo()).getLastName(),service.findOne(x.getFrom()).getLastName(),service.findOne(x.getTo()).getId(),service.findOne(x.getFrom()).getId(),x.getDate()))
                .collect(Collectors.toList());
        pendingRequestsHistoryModel.setAll(requestHistory);
    }

    private void initHistoryModel(List<FriendRequest> requests){
        List<FriendRequestDto> requestHistory = requests.stream()
                .map(x->new FriendRequestDto(x.getStatus(),service.findOne(x.getTo()).getLastName(),service.findOne(x.getFrom()).getLastName(),service.findOne(x.getTo()).getId(),service.findOne(x.getFrom()).getId(),x.getDate()))
                .collect(Collectors.toList());
        pendingRequestsHistoryModel.setAll(requestHistory);
    }

    private void initFriendsModel(){
        int pageNumber = Integer.parseInt(friendsPageCounter.getText());
        String name = friendSearchTextField.getText();
        Iterable<DTOUtilizator> friends = service.getFriends(pageNumber, pageSize, name);
        List<DTOUtilizator> friendsList = StreamSupport.stream(friends.spliterator(), false)
                .collect(Collectors.toList());
        friendsModel.setAll(friendsList);
    }

    private void initFriendsModel(List<DTOUtilizator> friends){
        friendsModel.setAll(friends);
    }

    private void setLabels() {
        firstNameLabel.setText(service.getUser().getFirstName());
        lastNameLabel.setText(service.getUser().getLastName());
        idLabel.setText(service.getUser().getId().toString());
        nrOfFriendsLabel.setText(service.getNrOfFriends().toString());
    }

    private void setLabels(String firstName, String lastName, String id, String nr) {
        firstNameLabel.setText(firstName);
        lastNameLabel.setText(lastName);
        idLabel.setText(id);
        nrOfFriendsLabel.setText(nr);
    }

    private void setRequestNotificationLabel(){
        long count = 0;
        for(Utilizator user : service.getPendingRequests()){
            count++;
        }
        if(count > 0){
            friendRequestNotificationLabel.setText(Long.toString(count));
            friendRequestNotificationLabel.setVisible(true);
        }
        else
            friendRequestNotificationLabel.setVisible(false);
    }

    @Override
    public void update(PrietenieChangeEvent prietenieChangeEvent) {
        switch (prietenieChangeEvent.getType()){
            case ADDUSER:
            case SENDREQUEST:
            case ACCEPTREQUEST:
            case DECLINEREQUEST:
            case DELETE:
                initMaxPages();
                initModel();
                initPendingModel();
                setLabels();
                initHistoryModel();
                initFriendsModel();
                setRequestNotificationLabel();
                initChoice();
                break;
            case SENTMESSAGE:
                initConversationsModel();
            case NOTIFICATION:
                showNotifications();
            case NEWEVENT:
                initAllEventsModel();
            case EVENTSUBSCRIPTION:
                initMyEventsModel();
                initAllEventsModel();
            case NEWGROUP:
                initGroupsModel();
            default:
                break;
        }
    }

    public void logOutButtonHandle(ActionEvent event) {
        try {
            scheduler.shutdown();
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("/views/logInView.fxml"));
            Parent root = loader.load();
            LogInController logInController = loader.getController();
            logInController.setLogInControllerService(service.getService());

            Scene logInScene = new Scene(root);

            Stage window = (Stage) ((Node)event.getSource()).getScene().getWindow();
            window.setScene(logInScene);
            window.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void allFriendsButtonHandle(ActionEvent event) {
        profileImageView.setImage(new Image(String.valueOf(getClass().getResource("/images/user.png"))));
        addFriendImageView.setImage(new Image(String.valueOf(getClass().getResource("/images/find_user.png"))));
        allFriendImageView.setImage(new Image(String.valueOf(getClass().getResource("/images/friends_selected.png"))));
        chatImageView.setImage(new Image(String.valueOf(getClass().getResource("/images/chat.png"))));
        requestsImageView.setImage(new Image(String.valueOf(getClass().getResource("/images/add_user.png"))));
        pdfImageView.setImage(new Image(String.valueOf(getClass().getResource("/images/icons8_pdf_50px_1.png"))));
        eventImageView.setImage(new Image(String.valueOf(getClass().getResource("/images/event.png"))));
        friendsPane.setVisible(true);
        userPane.setVisible(false);
        searchUserPane.setVisible(false);
        messagePane.setVisible(false);
        requestsPane.setVisible(false);
        pdfPane.setVisible(false);
        eventsPane.setVisible(false);
    }

    public void chatButtonHandle(ActionEvent event) {
        profileImageView.setImage(new Image(String.valueOf(getClass().getResource("/images/user.png"))));
        addFriendImageView.setImage(new Image(String.valueOf(getClass().getResource("/images/find_user.png"))));
        allFriendImageView.setImage(new Image(String.valueOf(getClass().getResource("/images/friends.png"))));
        chatImageView.setImage(new Image(String.valueOf(getClass().getResource("/images/chat_selected.png"))));
        requestsImageView.setImage(new Image(String.valueOf(getClass().getResource("/images/add_user.png"))));
        pdfImageView.setImage(new Image(String.valueOf(getClass().getResource("/images/icons8_pdf_50px_1.png"))));
        eventImageView.setImage(new Image(String.valueOf(getClass().getResource("/images/event.png"))));
//        notificationImageView.setImage(new Image(String.valueOf(getClass().getResource("/images/notification.png"))));
        friendsPane.setVisible(false);
        userPane.setVisible(false);
        searchUserPane.setVisible(false);
        messagePane.setVisible(true);
        requestsPane.setVisible(false);
        pdfPane.setVisible(false);
        eventsPane.setVisible(false);
    }

    public void addFriendButtonHandle(ActionEvent event) {
        profileImageView.setImage(new Image(String.valueOf(getClass().getResource("/images/user.png"))));
        addFriendImageView.setImage(new Image(String.valueOf(getClass().getResource("/images/find_user_selected.png"))));
        allFriendImageView.setImage(new Image(String.valueOf(getClass().getResource("/images/friends.png"))));
        chatImageView.setImage(new Image(String.valueOf(getClass().getResource("/images/chat.png"))));
        requestsImageView.setImage(new Image(String.valueOf(getClass().getResource("/images/add_user.png"))));
        pdfImageView.setImage(new Image(String.valueOf(getClass().getResource("/images/icons8_pdf_50px_1.png"))));
        eventImageView.setImage(new Image(String.valueOf(getClass().getResource("/images/event.png"))));
//        notificationImageView.setImage(new Image(String.valueOf(getClass().getResource("/images/notification.png"))));
        friendsPane.setVisible(false);
        userPane.setVisible(false);
        searchUserPane.setVisible(true);
        messagePane.setVisible(false);
        requestsPane.setVisible(false);
        pdfPane.setVisible(false);
        eventsPane.setVisible(false);
    }

    public void profileButtonHandle(ActionEvent event) {
        profileImageView.setImage(new Image(String.valueOf(getClass().getResource("/images/user_selected.png"))));
        addFriendImageView.setImage(new Image(String.valueOf(getClass().getResource("/images/find_user.png"))));
        allFriendImageView.setImage(new Image(String.valueOf(getClass().getResource("/images/friends.png"))));
        chatImageView.setImage(new Image(String.valueOf(getClass().getResource("/images/chat.png"))));
        pdfImageView.setImage(new Image(String.valueOf(getClass().getResource("/images/icons8_pdf_50px_1.png"))));
        requestsImageView.setImage(new Image(String.valueOf(getClass().getResource("/images/add_user.png"))));
        eventImageView.setImage(new Image(String.valueOf(getClass().getResource("/images/event.png"))));
//        notificationImageView.setImage(new Image(String.valueOf(getClass().getResource("/images/notification.png"))));
        friendsPane.setVisible(false);
        userPane.setVisible(true);
        searchUserPane.setVisible(false);
        messagePane.setVisible(false);
        requestsPane.setVisible(false);
        pdfPane.setVisible(false);
        eventsPane.setVisible(false);
    }

    public void checkProfileButtonHande(ActionEvent event) {
        try {
            PendingRequestUser user = allUsersTableView.getSelectionModel().getSelectedItem();
            if(user == null){
                AlertMessage.showErrorMessage(null, "No user was selected form the table!");
            }
            else {
//                scheduler.shutdown();
                Utilizator profileUser = user.getUser();


                FXMLLoader loader = new FXMLLoader();
                loader.setLocation(getClass().getResource("/views/profileView.fxml"));
                Parent root = loader.load();

                ProfileController controller = loader.getController();
                controller.setServiceAndUser(new ServiceWithUser(profileUser, service.getService()), service.getUser());

                Scene profileScene = new Scene(root);

                Stage window = (Stage) ((Node) event.getSource()).getScene().getWindow();
                window.setScene(profileScene);

                window.show();
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void AcceptRequestButtonHandle(ActionEvent event) {
        Utilizator user = pendingRequestsTableView.getSelectionModel().getSelectedItem();
        if(user == null){
            AlertMessage.showErrorMessage(null, "No user was selected!");
        }
        else{
            service.acceptRequest(user);
        }
    }

    public void requestsButtonHandle(ActionEvent event) {
        profileImageView.setImage(new Image(String.valueOf(getClass().getResource("/images/user.png"))));
        addFriendImageView.setImage(new Image(String.valueOf(getClass().getResource("/images/find_user.png"))));
        allFriendImageView.setImage(new Image(String.valueOf(getClass().getResource("/images/friends.png"))));
        chatImageView.setImage(new Image(String.valueOf(getClass().getResource("/images/chat.png"))));
        requestsImageView.setImage(new Image(String.valueOf(getClass().getResource("/images/add_user_selected.png"))));
        pdfImageView.setImage(new Image(String.valueOf(getClass().getResource("/images/icons8_pdf_50px_1.png"))));
        eventImageView.setImage(new Image(String.valueOf(getClass().getResource("/images/event.png"))));
//        notificationImageView.setImage(new Image(String.valueOf(getClass().getResource("/images/notification.png"))));
        friendsPane.setVisible(false);
        userPane.setVisible(false);
        searchUserPane.setVisible(false);
        messagePane.setVisible(false);
        requestsPane.setVisible(true);
        pdfPane.setVisible(false);
        eventsPane.setVisible(false);
    }

    public void declineRequestButtonHandle(ActionEvent event) {
        Utilizator user = pendingRequestsTableView.getSelectionModel().getSelectedItem();
        if(user == null){
            AlertMessage.showErrorMessage(null, "No user was selected!");
        }
        else{
            service.declineRequest(user);
        }
    }

    @FXML
    private TextField friendSearchTextField;

    public void searchFriendTextField(KeyEvent keyEvent) {
//        Predicate<DTOUtilizator> byName = x -> x.getLastName().startsWith(friendSearchTextField.getText());
//        List<DTOUtilizator> filteredFriends = StreamSupport.stream(service.getFriends().spliterator(), false)
//                .filter(byName)
//                .collect(Collectors.toList());
//        friendsModel.setAll(filteredFriends);
        initFriendsMaxPage();
        initFriendsModel();
    }

    public void unfriendButtonHandle(ActionEvent event) {
        DTOUtilizator friend = friendsTableView.getSelectionModel().getSelectedItem();
        if(friend == null){
            AlertMessage.showErrorMessage(null, "No user was selected from friends table!");
        }
        else
            service.removeFriend(friend);
    }

    public void checkProfileFriendsButtonHandle(ActionEvent event) {
        try {
            DTOUtilizator user = friendsTableView.getSelectionModel().getSelectedItem();
            if(user == null){
                AlertMessage.showErrorMessage(null, "No user was selected form the table!");
            }
            else {
                Utilizator profileUser = service.findOne(user.getId());


                FXMLLoader loader = new FXMLLoader();
                loader.setLocation(getClass().getResource("/views/profileView.fxml"));
                Parent root = loader.load();

                ProfileController controller = loader.getController();
                controller.setServiceAndUser(new ServiceWithUser(profileUser, service.getService()), service.getUser());

                Scene profileScene = new Scene(root);

                Stage window = (Stage) ((Node) event.getSource()).getScene().getWindow();
                window.setScene(profileScene);

                window.show();
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void searchAllUsersFilter(KeyEvent keyEvent) {
//        Predicate<PendingRequestUser> byName = x -> x.getLastName().startsWith(searchAllUsers.getText());
//        List<PendingRequestUser> filteredUsers = StreamSupport.stream(service.getPossibleFriends().spliterator(), false)
//                .filter(byName)
//                .collect(Collectors.toList());
//        allUsersModel.setAll(filteredUsers);
        initUsersMaxPage();
        initModel();
    }

    public void pdfButtonHandle(ActionEvent event) {
        profileImageView.setImage(new Image(String.valueOf(getClass().getResource("/images/user.png"))));
        addFriendImageView.setImage(new Image(String.valueOf(getClass().getResource("/images/find_user.png"))));
        allFriendImageView.setImage(new Image(String.valueOf(getClass().getResource("/images/friends.png"))));
        chatImageView.setImage(new Image(String.valueOf(getClass().getResource("/images/chat.png"))));
        requestsImageView.setImage(new Image(String.valueOf(getClass().getResource("/images/add_user.png"))));
        pdfImageView.setImage(new Image(String.valueOf(getClass().getResource("/images/icons8_pdf_50px_2.png"))));
        eventImageView.setImage(new Image(String.valueOf(getClass().getResource("/images/event.png"))));
//        notificationImageView.setImage(new Image(String.valueOf(getClass().getResource("/images/notification.png"))));
        friendsPane.setVisible(false);
        userPane.setVisible(false);
        searchUserPane.setVisible(false);
        messagePane.setVisible(false);
        requestsPane.setVisible(false);
        pdfPane.setVisible(true);
        eventsPane.setVisible(false);
    }

    public void eventButtonHandle(ActionEvent actionEvent) {
        profileImageView.setImage(new Image(String.valueOf(getClass().getResource("/images/user.png"))));
        addFriendImageView.setImage(new Image(String.valueOf(getClass().getResource("/images/find_user.png"))));
        allFriendImageView.setImage(new Image(String.valueOf(getClass().getResource("/images/friends.png"))));
        chatImageView.setImage(new Image(String.valueOf(getClass().getResource("/images/chat.png"))));
        requestsImageView.setImage(new Image(String.valueOf(getClass().getResource("/images/add_user.png"))));
        pdfImageView.setImage(new Image(String.valueOf(getClass().getResource("/images/icons8_pdf_50px_1.png"))));
        eventImageView.setImage(new Image(String.valueOf(getClass().getResource("/images/event_selected.png"))));
//        notificationImageView.setImage(new Image(String.valueOf(getClass().getResource("/images/notification.png"))));
        friendsPane.setVisible(false);
        userPane.setVisible(false);
        searchUserPane.setVisible(false);
        messagePane.setVisible(false);
        requestsPane.setVisible(false);
        pdfPane.setVisible(false);
        eventsPane.setVisible(true);
    }

    public void ViewPdf1ButtonHandle(ActionEvent event) {
        String strStart = pdf1StartDatePicker.getEditor().getText();
        String strEnd = pdf1EndDatePicker.getEditor().getText();
        if(strEnd.isEmpty() || strStart.isEmpty())
            AlertMessage.showErrorMessage(null, "Dates were not picked!");
        else {
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MM/d/yyyy");
            LocalDate startDate = pdf1StartDatePicker.getValue();
            LocalDate endDate = pdf1EndDatePicker.getValue();

            List<DTOUtilizator> list = new ArrayList<>();

            for(DTOUtilizator user : service.getFriends()){
                String date = user.getDate();
                LocalDateTime parsedDate = LocalDateTime.parse(date, Constants.DATE_TIME_FORMATTER);
                LocalDate localDate = parsedDate.toLocalDate();
                if(localDate.isAfter(startDate) && localDate.isBefore(endDate)){
                    list.add(user);
                }
            }

            Iterable<Message> mesaje = service.getMessagesToMe();

            List<MessageDTO> listMsg = StreamSupport.stream(service.getMessagesToMe().spliterator(), false)
                    .filter(x->x.getDate().toLocalDate().isAfter(startDate) && x.getDate().toLocalDate().isBefore(endDate))
                    .map(x->new MessageDTO(service.findOne(x.getFrom()).getLastName(), x.getMessage(), x.getDate(), "",x.getId()))
                    .collect(Collectors.toList());


            pdf1friendsModel.setAll(list);
            pdf1receivedMessagesModel.setAll(listMsg);

        }
    }

    public void GeneratePdf1ButtonHandle(ActionEvent event) {
        try {
            Document document = new Document();
            String fileName = service.getUser().getLastName() + "_1.pdf";
            PdfWriter.getInstance(document, new FileOutputStream(fileName));

            document.open();

            Paragraph msg1 = new Paragraph("Friends you added in this period: ");
            Paragraph space = new Paragraph(" ");
            PdfPTable friendsTable = new PdfPTable(2);
            PdfPCell nume = new PdfPCell(new Phrase("Nume"));
            PdfPCell data = new PdfPCell(new Phrase("Data"));
            friendsTable.addCell(nume);
            friendsTable.addCell(data);
            friendsTable.setHeaderRows(1);

            pdf1friendsModel.forEach(x->{
                PdfPCell name = new PdfPCell(new Phrase(x.getLastName()));
                PdfPCell date = new PdfPCell(new Phrase(x.getDate()));
                friendsTable.addCell(name);
                friendsTable.addCell(date);
            });

            Paragraph msg2 = new Paragraph("Messages you have received in this period:");
            PdfPTable messagesTable = new PdfPTable(3);

            PdfPCell from = new PdfPCell(new Phrase("From"));
            PdfPCell msg = new PdfPCell(new Phrase("Message"));
            messagesTable.addCell(from);
            messagesTable.addCell(data);
            messagesTable.addCell(msg);
            messagesTable.setHeaderRows(1);

            pdf1receivedMessagesModel.forEach(x->{
                PdfPCell fromName = new PdfPCell(new Phrase(x.getFrom()));
                PdfPCell date = new PdfPCell(new Paragraph(x.getDate().format(Constants.DATE_TIME_FORMATTER)));
                PdfPCell message = new PdfPCell(new Paragraph(x.getMsg()));
                messagesTable.addCell(fromName);
                messagesTable.addCell(date);
                messagesTable.addCell(message);
            });

            document.add(msg1);
            document.add(space);
            document.add(friendsTable);
            document.add(space);
            document.add(msg2);
            document.add(space);
            document.add(messagesTable);


            document.close();

        } catch (DocumentException | FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    public void Pdf2ViewButtonHandle(ActionEvent actionEvent) {
        String strStart = pdf2StartDate.getEditor().getText();
        String strEnd = pdf2EndDate.getEditor().getText();
        String name = pdf2ChoiceBox.getValue();
        if(strEnd.isEmpty() || strStart.isEmpty())
            AlertMessage.showErrorMessage(null, "Dates were not picked!");
        else if(name == null){
            AlertMessage.showErrorMessage(null, "Select user from choice box");
        }
        else {
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MM/d/yyyy");
            LocalDate startDate = pdf2StartDate.getValue();
            LocalDate endDate = pdf2EndDate.getValue();
            List<MessageDTO> listMsg = StreamSupport.stream(service.getMessagesToMe().spliterator(), false)
                    .filter(x->x.getDate().toLocalDate().isAfter(startDate) && x.getDate().toLocalDate().isBefore(endDate) && service.findOne(x.getFrom()).getLastName().equals(name))
                    .map(x->new MessageDTO(service.findOne(x.getFrom()).getLastName(), x.getMessage(), x.getDate(), "",x.getId()))
                    .collect(Collectors.toList());
            pdf2receivedMessagesModel.setAll(listMsg);
        }
        //TODO
    }

    public void Pdf2GenerateButoonHandle(ActionEvent actionEvent) {
        try {
            Document document = new Document();
            String fileName = service.getUser().getLastName() + "_2.pdf";
            PdfWriter.getInstance(document, new FileOutputStream(fileName));

            document.open();

            String name = pdf2ChoiceBox.getValue();
            String strStart = pdf2StartDate.getEditor().getText();
            String strEnd = pdf2EndDate.getEditor().getText();

            Paragraph msg1 = new Paragraph("Messages you have received from "+name+" from "+strStart+" to "+strEnd+": ");
            Paragraph space = new Paragraph(" ");

            PdfPTable messagesTable = new PdfPTable(2);

            PdfPCell data = new PdfPCell(new Phrase("Data"));
            PdfPCell msg = new PdfPCell(new Phrase("Message"));
            messagesTable.addCell(data);
            messagesTable.addCell(msg);
            messagesTable.setHeaderRows(1);

            pdf2receivedMessagesModel.forEach(x->{
                PdfPCell date = new PdfPCell(new Paragraph(x.getDate().format(Constants.DATE_TIME_FORMATTER)));
                PdfPCell message = new PdfPCell(new Paragraph(x.getMsg()));
                messagesTable.addCell(date);
                messagesTable.addCell(message);
            });


            document.add(msg1);
            document.add(space);
            document.add(messagesTable);


            document.close();

        } catch (DocumentException | FileNotFoundException e) {
            e.printStackTrace();
        }
    }

//    public void notificationButtonHandle(ActionEvent actionEvent) {
//
//    }

    public void CreateNewEventButtonHandle(ActionEvent actionEvent) {
        try {
            // create a new stage for the popup dialog.
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("/views/newEventView.fxml"));

            Parent root = loader.load();

            NewEventController controller = loader.getController();
            controller.setService(service);

            // Create the dialog Stage.
            Stage dialogStage = new Stage();
            dialogStage.initModality(Modality.NONE);
            //dialogStage.initOwner(primaryStage);
            Scene scene = new Scene(root);
            dialogStage.setScene(scene);

            dialogStage.show();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void EventDetailsButtonHandle(ActionEvent actionEvent) {
        try {
            Event ev = null;
            Event ev1 = allEventsTableView.getSelectionModel().getSelectedItem();
            Event ev2 = myEventsTableView.getSelectionModel().getSelectedItem();
            if(ev1 == null && ev2 != null){
                ev = myEventsTableView.getSelectionModel().getSelectedItem();
            }
            else if(ev1 != null && ev2 == null){
                ev = allEventsTableView.getSelectionModel().getSelectedItem();
            }
            if(ev == null){
                AlertMessage.showErrorMessage(null, "No event was selected!\n");
            }
            else{
            // create a new stage for the popup dialog.
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("/views/EventDetailsView.fxml"));

            Parent root = loader.load();

            EventDetailsController controller = loader.getController();
            controller.setService(service, ev);

            // Create the dialog Stage.
            Stage dialogStage = new Stage();
            dialogStage.initModality(Modality.NONE);
            //dialogStage.initOwner(primaryStage);
            Scene scene = new Scene(root);
            dialogStage.setScene(scene);

            dialogStage.show();
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void CleareAllEvents(MouseEvent mouseEvent) {
        allEventsTableView.getSelectionModel().clearSelection();
    }

    public void ClearMyEvents(MouseEvent mouseEvent) {
        myEventsTableView.getSelectionModel().clearSelection();
    }

    public void showConversationWithUser(MouseEvent mouseEvent) {
        replyOneButton.setVisible(false);
        groupTableView.getSelectionModel().clearSelection();
        messagesModel.clear();
        Utilizator user = userConversationTableView.getSelectionModel().getSelectedItem();
        if(user != null){
            initMessageMaxPage();
            messagePageCounter.setText(Integer.toString(maxMessagePage));
            initMessageModel(user, maxMessagePage);
            sendMessageBox.setVisible(true);
        }
        else{
            userConversationTableView.getSelectionModel().clearSelection();
            sendMessageBox.setVisible(false);
        }
    }

    public void sendMessageButtonHandle(ActionEvent actionEvent) {
        Utilizator user = userConversationTableView.getSelectionModel().getSelectedItem();
        Group gr = groupTableView.getSelectionModel().getSelectedItem();
        int grIndex = groupTableView.getSelectionModel().getSelectedIndex();
        MessageDTO msg = messageTableView.getSelectionModel().getSelectedItem();
        String message = messageTextArea.getText();
        if(message.isEmpty()){
            AlertMessage.showErrorMessage(null, "You must type a message!\n");
        }
        else{
            if(user != null) {
                if (msg == null) {
                    service.sendMessageToUser(message, user.getId());
                } else {
                    service.replyMessageFromUser(message, msg.getId());
                }
                initMessageMaxPage();
                messagePageCounter.setText(Integer.toString(maxMessagePage));
                initMessageModel(user, maxMessagePage);
                messageTextArea.clear();
            }
            else{
                if(msg == null){
                    service.sendMessageToGroup(message, gr.getId());
                }
                else{
                    service.replyMessageFromGroup(message, msg.getId());
                }
                groupTableView.getSelectionModel().select(grIndex);
                initMessageMaxPage();
                messagePageCounter.setText(Integer.toString(maxMessagePage));
                initMessageModel(gr, maxMessagePage);
                messageTextArea.clear();
            }
        }
    }

    public void newGroupButtonHandle(ActionEvent actionEvent) {
        try {
            // create a new stage for the popup dialog.
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("/views/createNewGroupView.fxml"));

            Parent root = loader.load();

            NewGroupController controller = loader.getController();
            controller.setService(service);

            // Create the dialog Stage.
            Stage dialogStage = new Stage();
            dialogStage.initModality(Modality.NONE);
            //dialogStage.initOwner(primaryStage);
            Scene scene = new Scene(root);
            dialogStage.setScene(scene);

            dialogStage.show();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void clearTableSelections(MouseEvent mouseEvent) {
        messageTableView.getSelectionModel().clearSelection();
        replyOneButton.setVisible(false);
    }

    public void showGroupConversation(MouseEvent mouseEvent) {
        Group group = groupTableView.getSelectionModel().getSelectedItem();
        replyOneButton.setVisible(false);
        messagesModel.clear();
        userConversationTableView.getSelectionModel().clearSelection();
        if(group != null){
            initMessageMaxPage();
            messagePageCounter.setText(Integer.toString(maxMessagePage));
            if(maxMessagePage > 0){
                initMessageModel(group, maxMessagePage);
                sendMessageBox.setVisible(true);
            }
        }
        else{
            userConversationTableView.getSelectionModel().clearSelection();
            sendMessageBox.setVisible(false);
        }
    }

    public void setReplyButton(MouseEvent mouseEvent) {
        replyOneButton.setVisible(messageTableView.getSelectionModel().getSelectedItem() != null && groupTableView.getSelectionModel().getSelectedItem() != null);
    }

    public void replyOneButtonHandle(ActionEvent actionEvent) {
        String msg = messageTextArea.getText();
        if(msg.isEmpty()){
            AlertMessage.showErrorMessage(null, "Please type a message!\n");
        }
        else{
            MessageDTO m = messageTableView.getSelectionModel().getSelectedItem();
            service.replyMessageFromUser(msg, m.getId());
            messageTextArea.clear();
        }
    }

    @FXML
    private TextField friendsPageCounter;
    private final int pageSize = 2;
    private int maxFriendsPage;
    private int maxUsersPage;
    private int maxRequestsPage;
    private int maxMessagePage;

    @FXML
    private TextField usersPageCounter;

    @FXML
    private TextField requestsPageCounter;

    @FXML
    private TextField messagePageCounter;

    private void initMaxPages(){
        initFriendsMaxPage();
        initUsersMaxPage();
        initRequestsMaxPage();
    }

    private void initMessageMaxPage() {
        int messages = 0;
        Utilizator user = userConversationTableView.getSelectionModel().getSelectedItem();
        Group group = groupTableView.getSelectionModel().getSelectedItem();
        if(user != null){
            for(MessageDTO ignored : service.getMessagesWithUser(user.getId())){
                messages++;
            }
        }
        else{
            for(MessageDTO ignored : service.getMessagesWithGroup(group.getId())){
                messages++;
            }
        }
        if(messages % pageSize > 0){
            maxMessagePage = messages / pageSize + 1;
        }
        else{
            maxMessagePage = messages / pageSize;
        }

    }

    private void initRequestsMaxPage(){
        int requests = 0;
        for(FriendRequest ignored : service.getFriendRequests()){
            requests++;
        }
        if(requests % pageSize > 0){
            maxRequestsPage = requests/pageSize + 1;
        }
        else{
            maxRequestsPage = requests/pageSize;
        }
    }

    private void initUsersMaxPage() {
        int users = 0;
        String name = searchAllUsers.getText();
        for(PendingRequestUser user : service.getPossibleFriends()){
            if(user.getLastName().startsWith(name))
                users++;
        }
        if(users % pageSize > 0){
            maxUsersPage = users/pageSize + 1;
        }
        else{
            maxUsersPage = users/pageSize;
        }
    }

    private void initFriendsMaxPage() {
        int friends = 0;
        String name = friendSearchTextField.getText();
        for(DTOUtilizator user : service.getFriends()){
            if(user.getLastName().startsWith(name))
                friends++;
        }
        if(friends % pageSize > 0){
            maxFriendsPage = friends/pageSize + 1;
        }
        else
            maxFriendsPage = friends/pageSize;
    }

    public void decFriendsPage(ActionEvent actionEvent) {
        int pageNumber = Integer.parseInt(friendsPageCounter.getText());
        if(pageNumber - 1 > 0){
            friendsPageCounter.setText(Integer.toString(pageNumber-1));
            initFriendsModel();
        }
    }

    public void incFriendsPage(ActionEvent actionEvent) {
        int pageNumber = Integer.parseInt(friendsPageCounter.getText());
        if(pageNumber + 1 <= maxFriendsPage){
            friendsPageCounter.setText(Integer.toString(pageNumber+1));
            initFriendsModel();
        }
    }

    public void decUsersPage(ActionEvent actionEvent) {
        int pageNumber = Integer.parseInt(usersPageCounter.getText());
        if(pageNumber - 1 > 0){
            usersPageCounter.setText(Integer.toString(pageNumber-1));
            initModel();
        }
    }

    public void incUsersPage(ActionEvent actionEvent) {
        int pageNumber = Integer.parseInt(usersPageCounter.getText());
        if(pageNumber + 1 <= maxUsersPage){
            usersPageCounter.setText(Integer.toString(pageNumber+1));
            initModel();
        }
    }

    public void decRequestsPage(ActionEvent actionEvent) {
        int pageNumber = Integer.parseInt(requestsPageCounter.getText());
        if(pageNumber - 1 > 0){
            requestsPageCounter.setText(Integer.toString(pageNumber-1));
            initHistoryModel();
        }
    }

    public void incRequestsPage(ActionEvent actionEvent) {
        int pageNumber = Integer.parseInt(requestsPageCounter.getText());
        if(pageNumber + 1 <= maxRequestsPage){
            requestsPageCounter.setText(Integer.toString(pageNumber+1));
            initHistoryModel();
        }
    }


    public void messageScrollHandle(ScrollEvent scrollEvent) {
        System.out.println("print");
        System.out.println(maxMessagePage);
        Utilizator user = userConversationTableView.getSelectionModel().getSelectedItem();
        Group gr = groupTableView.getSelectionModel().getSelectedItem();
        int pageNumber = Integer.parseInt(messagePageCounter.getText());
        if(scrollEvent.getDeltaY()>0) {
            if(pageNumber + 1 <= maxMessagePage){
                messagePageCounter.setText(Integer.toString(++pageNumber));
                if(user != null)
                    initMessageModel(user, pageNumber);
                else
                    initMessageModel(gr, pageNumber);
            }
        }
        else{
            if(pageNumber - 1 >= 1 ) {
                messagePageCounter.setText(Integer.toString(--pageNumber));
                if(user != null)
                    initMessageModel(user, pageNumber);
                else
                    initMessageModel(gr, pageNumber);
            }
        }
    }
}